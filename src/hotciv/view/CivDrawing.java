package hotciv.view;

import hotciv.framework.*;
import minidraw.framework.*;
import minidraw.standard.ImageFigure;
import minidraw.standard.StandardDrawing;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * CivDrawing is a specialized Drawing (model component) from
 * MiniDraw that dynamically builds the list of Figures for MiniDraw
 * to render the Unit and other information objects that are visible
 * in the Game instance.
 * <p>
 * This is a TEMPLATE for the dSoftArk Exercise! This means
 * that it is INCOMPLETE and that there are several options
 * for CLEANING UP THE CODE when you add features to it!
 * <p>
 * This source code is from the book
 * "Flexible, Reliable Software:
 * Using Patterns and Agile Development"
 * published 2010 by CRC Press.
 * Author:
 * Henrik B Christensen
 * Department of Computer Science
 * Aarhus University
 * <p>
 * Please visit http://www.baerbak.com/ for further information.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class CivDrawing
        implements Drawing, GameObserver {

    private final Point workForcePosition = new Point(GfxConstants.WORKFORCEFOCUS_X, GfxConstants.WORKFORCEFOCUS_Y);
    private final Point cityShieldPosition = new Point(GfxConstants.CITY_SHIELD_X, GfxConstants.CITY_SHIELD_Y);
    private final Point cityProductionPoint = new Point(GfxConstants.CITY_PRODUCTION_X, GfxConstants.CITY_PRODUCTION_Y);
    private final Point unitMoveCountPosition = new Point(GfxConstants.UNIT_COUNT_X, GfxConstants.UNIT_COUNT_Y);
    private final Point unitShieldPosition = new Point(GfxConstants.UNIT_SHIELD_X, GfxConstants.UNIT_SHIELD_Y);
    private final Point unitTypePosition = new Point(GfxConstants.UNIT_TYPE_X, GfxConstants.UNIT_TYPE_Y);
    /**
     * the Game instance that this UnitDrawing is going to render units
     * from
     */
    protected Game game;

    protected Map<Position, List<Figure>> positionFigureMap = null;
    private Drawing delegate;
    private ImageFigure turnShieldIcon;
    private ImageFigure cityImageFigure;
    private ImageFigure cityProductionImageFigure;
    private ImageFigure workforceImageFigure;
    private TextFigure unitCountTextFigure;
    private ImageFigure unitShieldImageFigure;
    private ImageFigure unitTypeImageFigure;

    public CivDrawing(DrawingEditor editor, Game game) {
        super();
        this.delegate = new StandardDrawing();
        this.game = game;

        // register this unit drawing as listener to any game state
        // changes...
        game.addObserver(this);
        // ... and build up the set of figures associated with
        // units in the game.
        defineUnitMap();
        // and the set of 'icons' in the status panel
        defineIcons();
    }

    /**
     * The CivDrawing should not allow client side
     * units to add and manipulate figures; only figures
     * that renders game objects are relevant, and these
     * should be handled by observer events from the game
     * instance. Thus this method is 'killed'.
     */
    public Figure add(Figure arg0) {
        throw new RuntimeException("Should not be used...");
    }

    /**
     * erase the old list of units, and build a completely new
     * one from scratch by iterating over the game world and add
     * Figure instances for each unit in the world.
     */
    private void defineUnitMap() {
        // ensure no units of the old list are accidental in
        // the selection!
        clearSelection();
        positionFigureMap = new HashMap<>();

        Position p;
        for (int r = 0; r < GameConstants.WORLDSIZE; r++) {
            for (int c = 0; c < GameConstants.WORLDSIZE; c++) {
                p = new Position(r, c);
                positionFigureMap.put(p, new ArrayList<>());
                City city = game.getCityAt(p);
                if (city != null) {
                    putCity(p, city);
                }

                Unit unit = game.getUnitAt(p);
                if (unit != null) {
                    putUnit(p, unit);
                }
            }
        }
    }

    private void defineIcons() {
        // very much a template implementation :)
        turnShieldIcon =
                new ImageFigure("redshield",
                        new Point(GfxConstants.TURN_SHIELD_X,
                                GfxConstants.TURN_SHIELD_Y));
        // insert in delegate figure list to ensure graphical
        // rendering.
        delegate.add(turnShieldIcon);

        cityImageFigure = new ImageFigure(GfxConstants.NOTHING, cityShieldPosition);
        delegate.add(turnShieldIcon);
    }

    // === Observer Methods ===

    public void worldChangedAt(Position pos) {
        System.out.println("CivDrawing: world changes at " + pos);
        clearSelection();

        List<Figure> figures = positionFigureMap.get(pos);
        for (Figure figure : figures) {
            delegate.remove(figure);
        }

        positionFigureMap.put(pos, new ArrayList<>());

        City cityAt = game.getCityAt(pos);
        if (cityAt != null) {
            putCity(pos, cityAt);
        }
        Unit unitAt = game.getUnitAt(pos);
        if (unitAt != null) {
            putUnit(pos, unitAt);
        }
    }

    private void putCity(Position pos, City cityAt) {
        Point point = new Point(GfxConstants.getXFromColumn(pos.getColumn()), GfxConstants.getYFromRow(pos.getRow()));
        CityFigure cityFigure = new CityFigure(cityAt, point);
        cityFigure.addFigureChangeListener(this);
        delegate.add(cityFigure);
        positionFigureMap.get(pos).add(cityFigure);
    }

    private void putUnit(Position pos, Unit unitAt) {
        String type = unitAt.getTypeString();
        Point point = new Point(GfxConstants.getXFromColumn(pos.getColumn()),
                GfxConstants.getYFromRow(pos.getRow()));
        UnitFigure unitFigure =
                new UnitFigure(type, point, unitAt);
        unitFigure.addFigureChangeListener(this);
        positionFigureMap.get(pos).add(unitFigure);
        delegate.add(unitFigure);
    }

    public void turnEnds(Player nextPlayer, int age) {
        System.out.println("CivDrawing: turnEnds for " +
                nextPlayer + " at " + age);
        String playername = "red";
        if (nextPlayer == Player.BLUE) {
            playername = "blue";
        }
        turnShieldIcon.set(playername + "shield",
                new Point(GfxConstants.TURN_SHIELD_X,
                        GfxConstants.TURN_SHIELD_Y));
    }

    public void tileFocusChangedAt(Position position) {
        delegate.remove(cityImageFigure);
        if (cityProductionImageFigure != null) {
            delegate.remove(cityProductionImageFigure);
        }
        if (workforceImageFigure != null) {
            delegate.remove(workforceImageFigure);
        }
        if (unitCountTextFigure != null && unitShieldImageFigure != null && unitTypeImageFigure != null) {
            delegate.remove(unitCountTextFigure);
            delegate.remove(unitShieldImageFigure);
            delegate.remove(unitTypeImageFigure);
        }

        City city = game.getCityAt(position);
        if (city != null) {
            cityImageFigure = new ImageFigure((city.getOwner().equals(Player.RED) ? GfxConstants.RED_SHIELD : GfxConstants.BLUE_SHIELD), cityShieldPosition);
            delegate.add(cityImageFigure);
            if (city.getProduction() != null && city.getProduction().length() > 0) {
                cityProductionImageFigure = new ImageFigure(city.getProduction(), cityProductionPoint);
                delegate.add(cityProductionImageFigure);
            }
            if (city.getWorkforceFocus() != null && city.getWorkforceFocus().length() > 0) {
                workforceImageFigure = new ImageFigure(city.getWorkforceFocus(), workForcePosition);
                delegate.add(workforceImageFigure);
            }
        }
        Unit unit = game.getUnitAt(position);
        if (unit != null) {
            int moveCount = unit.getMoveCount();
            unitCountTextFigure = new TextFigure(Integer.toString(moveCount), unitMoveCountPosition);
            unitShieldImageFigure = new ImageFigure((unit.getOwner().equals(Player.RED) ? GfxConstants.RED_SHIELD : GfxConstants.BLUE_SHIELD), unitShieldPosition);
            unitTypeImageFigure = new ImageFigure(unit.getTypeString(), unitTypePosition);
            delegate.add(unitCountTextFigure);
            delegate.add(unitShieldImageFigure);
            delegate.add(unitTypeImageFigure);
        }


    }

    @Override
    public void addToSelection(Figure arg0) {
        delegate.addToSelection(arg0);
    }

    @Override
    public void clearSelection() {
        delegate.clearSelection();
    }

    @Override
    public void removeFromSelection(Figure arg0) {
        delegate.removeFromSelection(arg0);
    }

    @Override
    public List<Figure> selection() {
        return delegate.selection();
    }

    @Override
    public void toggleSelection(Figure arg0) {
        delegate.toggleSelection(arg0);
    }

    @Override
    public void figureChanged(FigureChangeEvent arg0) {
        delegate.figureChanged(arg0);
    }

    @Override
    public void figureInvalidated(FigureChangeEvent arg0) {
        delegate.figureInvalidated(arg0);
    }

    @Override
    public void figureRemoved(FigureChangeEvent arg0) {
        delegate.figureRemoved(arg0);
    }

    @Override
    public void figureRequestRemove(FigureChangeEvent arg0) {
        delegate.figureRequestRemove(arg0);
    }

    @Override
    public void figureRequestUpdate(FigureChangeEvent arg0) {
        delegate.figureRequestUpdate(arg0);
    }

    @Override
    public void addDrawingChangeListener(DrawingChangeListener arg0) {
        delegate.addDrawingChangeListener(arg0);
    }

    @Override
    public void removeDrawingChangeListener(DrawingChangeListener arg0) {
        delegate.removeDrawingChangeListener(arg0);
    }

    @Override
    public Figure findFigure(int arg0, int arg1) {

        return delegate.findFigure(arg0, arg1);
    }

    @Override
    public Iterator<Figure> iterator() {

        return delegate.iterator();
    }

    @Override
    public void lock() {
        delegate.lock();
    }

    @Override
    public Figure remove(Figure arg0) {

        return delegate.remove(arg0);
    }

    @Override
    public void requestUpdate() {
        delegate.requestUpdate();

    }

    @Override
    public void unlock() {
        delegate.unlock();

    }
}
