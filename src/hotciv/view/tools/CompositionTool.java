package hotciv.view.tools;

import minidraw.framework.DrawingEditor;
import minidraw.framework.Tool;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.List;

public class CompositionTool implements Tool {


    private List<Tool> tools;

    public CompositionTool(DrawingEditor editor, List<Tool> tools) {
        this.tools = tools;
    }

    @Override
    public void mouseDown(MouseEvent mouseEvent, int x, int y) {
        for (Tool tool : tools) {
            tool.mouseDown(mouseEvent, x, y);
        }
    }

    @Override
    public void mouseDrag(MouseEvent mouseEvent, int x, int y) {
        for (Tool tool : tools) {
            tool.mouseDrag(mouseEvent, x, y);
        }
    }

    @Override
    public void mouseUp(MouseEvent mouseEvent, int x, int y) {
        for (Tool tool : tools) {
            tool.mouseUp(mouseEvent, x, y);
        }
    }

    @Override
    public void mouseMove(MouseEvent mouseEvent, int x, int y) {
        for (Tool tool : tools) {
            tool.mouseMove(mouseEvent, x, y);
        }
    }

    @Override
    public void keyDown(KeyEvent keyEvent, int i) {
        for (Tool tool : tools) {
            tool.keyDown(keyEvent, i);
        }
    }
}
