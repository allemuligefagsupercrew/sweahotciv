package hotciv.view.tools;

import hotciv.framework.Game;
import hotciv.framework.Position;
import hotciv.view.GfxConstants;
import hotciv.view.TextFigure;
import minidraw.framework.DrawingEditor;
import minidraw.standard.AbstractTool;

import java.awt.event.MouseEvent;

public class SetFocusTool extends AbstractTool{
    private Game game;

    public SetFocusTool(DrawingEditor editor, Game game) {
        super(editor);
        this.game = game;
    }

    @Override
    public void mouseDown(MouseEvent mouseEvent, int x, int y) {

        Position click = GfxConstants.getPositionFromXY(x, y);

        game.setTileFocus(click);
    }

}
