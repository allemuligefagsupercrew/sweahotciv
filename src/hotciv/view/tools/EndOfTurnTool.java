package hotciv.view.tools;

import hotciv.framework.Game;
import hotciv.view.GfxConstants;
import minidraw.framework.DrawingEditor;
import minidraw.standard.AbstractTool;

import java.awt.event.MouseEvent;

public class EndOfTurnTool extends AbstractTool {
    private Game game;

    public EndOfTurnTool(DrawingEditor editor, Game game) {
        super(editor);
        this.game = game;
    }

    @Override
    public void mouseDown(MouseEvent mouseEvent, int x, int y) {
        if (GfxConstants.TURN_SHIELD_X <= x && x <= GfxConstants.TURN_SHIELD_X + 27 &&
                GfxConstants.TURN_SHIELD_Y <= y && y <= GfxConstants.TURN_SHIELD_Y + 39) {
            game.endOfTurn();
        }
    }
}
