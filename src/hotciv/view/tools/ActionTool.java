package hotciv.view.tools;

import hotciv.framework.Game;
import hotciv.framework.Position;
import hotciv.view.GfxConstants;
import javafx.event.EventTarget;
import minidraw.framework.DrawingEditor;
import minidraw.standard.AbstractTool;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class ActionTool extends AbstractTool {
    private Game game;

    public ActionTool(DrawingEditor editor, Game game) {
        super(editor);
        this.game = game;
    }

    @Override
    public void mouseDown(MouseEvent mouseEvent, int x, int y) {
       Position position = GfxConstants.getPositionFromXY(x,y);

        if(mouseEvent.isShiftDown() && game.getUnitAt(position) != null) {
            game.performUnitActionAt(position);
        }
    }
}
