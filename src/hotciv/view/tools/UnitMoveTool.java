package hotciv.view.tools;

import hotciv.framework.Game;
import hotciv.framework.Position;
import hotciv.view.GfxConstants;
import minidraw.framework.DrawingEditor;
import minidraw.standard.AbstractTool;
import minidraw.standard.SelectionTool;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class UnitMoveTool extends AbstractTool {
    private Game game;

    public UnitMoveTool(DrawingEditor editor, Game game) {
        super(editor);
        this.game = game;

    }

    @Override
    public void mouseUp(MouseEvent mouseEvent, int x, int y) {

        Position click = GfxConstants.getPositionFromXY(fAnchorX, fAnchorY);

        if (game.getUnitAt(click) != null){
            game.moveUnit(click, GfxConstants.getPositionFromXY(x, y));
        }
    }

}
