package hotciv.framework;

import hotciv.standard.GameImpl;

/**
 * Strategy that is executed upon the capture of a city
 */
public interface WinStrategy {

    /**
     * Method run when strategy is activated.
     * Takes game and city as parameters. This is so the strategy knows what city has been captured, and are able to read or modify the game.
     *
     * @param game - the instance of the game being played.
     */
    Player getWinner(GameImpl game);
}
