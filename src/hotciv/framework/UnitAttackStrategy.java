package hotciv.framework;

import hotciv.standard.GameImpl;

/**
 * Created by Johannes on 01-10-2017.
 */
public interface UnitAttackStrategy {

    /**
     * PRECONDITION: both input positions has units
     *
     * @param game         implementation of the game
     * @param attackerPos  position for friendly unit
     * @param defendantPos position for hostile unit
     * @return whether friendly destroyed hostile unit
     */
    public boolean getOutcome(GameImpl game, Position attackerPos, Position defendantPos);

}
