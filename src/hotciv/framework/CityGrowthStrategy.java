package hotciv.framework;

import hotciv.standard.CityImpl;

/**
 * Strategy to determine the city growth
 */
public interface CityGrowthStrategy {

    /**
     * Handles growth of the city
     *
     * @param city the relevant city
     */
    public void handleGrowth(CityImpl city);

}
