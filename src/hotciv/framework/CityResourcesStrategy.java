package hotciv.framework;

import hotciv.standard.GameImpl;

/**
 * Strategy to determine the amount of resources to be added to the city
 */
public interface CityResourcesStrategy {

    /**
     * @param game     the game Object
     * @param position the position of the {@link hotciv.framework.City} in question
     * @return an integer representing the amount of food to be added to the city
     */
    public int calculateFood(GameImpl game, Position position);

    /**
     * @param game     the game Object
     * @param position the position of the {@link hotciv.framework.City} in question
     * @return an integer representing the amount of food to be added to the city
     */
    public int calculateProduction(GameImpl game, Position position);
}
