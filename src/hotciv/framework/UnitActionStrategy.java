package hotciv.framework;

import hotciv.standard.GameImpl;

public interface UnitActionStrategy {

    /**
     * @param game the game object the unit is present in
     * @param p    the position of the unit
     */
    void performUnitActionAt(GameImpl game, Position p);

}
