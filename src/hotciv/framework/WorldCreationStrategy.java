package hotciv.framework;


import hotciv.standard.UnitImpl;

import java.util.Map;

/**
 * Strategy used in creating the world map.
 */
public interface WorldCreationStrategy {
    /**
     * @return stringarray representing a world-layout
     */
    public String[] getWorldLayout();

    /**
     * @return the cityMap for the initial game setup
     */
    public Map<Position, City> getCityMap();

    /**
     * @return the map between objects of type {@link Position} and {@link UnitImpl} describing the position of the units
     */
    public Map<Position, UnitImpl> getUnitMap();
}
