package hotciv.framework;

public interface TranscribingObserver {

    /**
     * Activates the {@link TranscribingObserver}
     */
    void activate();

    /**
     * Activates the {@link TranscribingObserver}
     */
    void deactivate();

    /**
     * Notification about unit movement
     *
     * @param from position unit is moved from
     * @param to   position unit is moved to
     */
    void notifyUnitMove(Position from, Position to);

    /**
     * Notification about change in city ownership
     *
     * @param pos position of city
     */
    void notifyCityHasNewOwner(Position pos);

    /**
     * Notification about creation of {@link Unit}
     *
     * @param pos position unit is spawned at
     */
    void notifyUnitCreation(Position pos);

    /**
     * Notification on end of turn
     */
    void notifyEndOfTurn();

    /**
     * Notification upon update of winner
     */
    void notifyWinnerUpdated();

    /**
     * Notification upon change in city focus
     *
     * @param pos position of city
     */
    void notifyChangeOfFocus(Position pos);

    /**
     * Notification upon change in city production
     *
     * @param pos city position
     */
    void notifyProductionChange(Position pos);

    /**
     * Notification upon unit performing unitaction
     * <p>
     * Unit must be taken from parameter as unit can be removed upon action being performed
     *
     * @param pos    position of unit
     * @param unitAt unit performing action
     */
    void notifyUnitAction(Position pos, Unit unitAt);
}
