package hotciv.framework;

public interface DieRollStrategy {

    /**
     * Interface used when imitating rolling a die
     * @return integer value from dieroll
     */
    public int rollDie();
}
