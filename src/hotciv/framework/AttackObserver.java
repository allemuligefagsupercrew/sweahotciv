package hotciv.framework;

public interface AttackObserver {

    /**
     * Notifies observer about an attack.
     *
     * @param player - the player who is the subject of the notification
     */
    public void notifyObserver(Player player);
}
