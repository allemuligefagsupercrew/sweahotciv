package hotciv.framework;

/**
 * Strategy used to determine how aging works in the CivilizationGame
 */
public interface AgingStrategy {

    /**
     * Method that takes old age and returns new one.
     *
     * @param age the age from which the game is aging from
     * @return new age
     */
    public int getNewAge(int age);
}
