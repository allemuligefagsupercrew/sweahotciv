package hotciv.framework;

import hotciv.standard.GameImpl;

/**
 * Factory for getting setup specific strategies
 */
public interface Factory {

    /**
     * @return {@link AgingStrategy} for the setup
     */
    public AgingStrategy createAgingStrategy();

    /**
     * @return {@link UnitActionStrategy} for the setup
     */
    public UnitActionStrategy createUnitActionStrategy();

    /**
     * @return {@link UnitAttackStrategy} for the setup
     */
    public UnitAttackStrategy createUnitAttackStrategy();

    /**
     * @return {@link WinStrategy} for the setup
     */
    public WinStrategy createWinStrategy(GameImpl game);

    /**
     * @return {@link WorldCreationStrategy} for the setup
     */
    public WorldCreationStrategy createWorldCreationStrategy();

    /**
     * @return {@link CityGrowthStrategy} for the setup
     * @param game relevant game
     */
    public CityGrowthStrategy createCityGrowthStrategy(GameImpl game);

    /**
     * @return {@link CityResourcesStrategy} for the setup
     */
    public CityResourcesStrategy createCityResourcesStrategy();


}
