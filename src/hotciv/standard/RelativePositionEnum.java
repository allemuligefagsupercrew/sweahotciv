package hotciv.standard;

import hotciv.framework.Position;

public enum RelativePositionEnum {
    N, NE, E, SE, S, SW, W, NW;

    public static Position getOffset(RelativePositionEnum val) {
        switch (val) {
            case N:
                return new Position(-1, 0);
            case NE:
                return new Position(-1, 1);
            case E:
                return new Position(0, 1);
            case SE:
                return new Position(1, 1);
            case S:
                return new Position(1, 0);
            case SW:
                return new Position(1, -1);
            case W:
                return new Position(0, -1);
            case NW:
                return new Position(-1, -1);
            default:
                return null;
        }

    }
}
