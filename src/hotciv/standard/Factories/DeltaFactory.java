package hotciv.standard.Factories;

import hotciv.framework.*;
import hotciv.standard.GameImpl;
import hotciv.standard.strategy.*;

public class DeltaFactory implements Factory {
    /**
     * @return {@link AgingStrategy} for the setup
     */
    @Override
    public AgingStrategy createAgingStrategy() {
        return new AlphaCivAgingStrategy();
    }

    /**
     * @return {@link UnitActionStrategy} for the setup
     */
    @Override
    public UnitActionStrategy createUnitActionStrategy() {
        return new VoidUnitActionStrategy();
    }

    /**
     * @return {@link UnitAttackStrategy} for the setup
     */
    @Override
    public UnitAttackStrategy createUnitAttackStrategy() {
        return new AlphaCivUnitAttackStrategy();
    }

    /**
     * @return {@link WinStrategy} for the setup
     */
    @Override
    public WinStrategy createWinStrategy(GameImpl game) {
        return new AlphaCivWinStrategy();
    }

    /**
     * @return {@link WorldCreationStrategy} for the setup
     */
    @Override
    public WorldCreationStrategy createWorldCreationStrategy() {
        return new DeltaCivWorldCreationStrategy();
    }


    /**
     * @param game
     * @return {@link CityGrowthStrategy} for the setup
     */
    @Override
    public CityGrowthStrategy createCityGrowthStrategy(GameImpl game) {
        return new AlphaCityGrowthStrategy();
    }

    /**
     * @return {@link CityResourcesStrategy} for the setup
     */
    @Override
    public CityResourcesStrategy createCityResourcesStrategy() {
        return new AlphaCivResourcesStrategy();
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
