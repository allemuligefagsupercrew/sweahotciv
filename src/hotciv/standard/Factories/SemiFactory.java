package hotciv.standard.Factories;

import hotciv.framework.*;
import hotciv.standard.GameImpl;
import hotciv.standard.strategy.*;

public class SemiFactory implements Factory {

    private DieRollStrategy dieRollStrategy;

    public SemiFactory(DieRollStrategy dieRollStrategy) {
        this.dieRollStrategy = dieRollStrategy;
    }

    /**
     * @return {@link AgingStrategy} for the setup
     */
    @Override
    public AgingStrategy createAgingStrategy() {
        return new BetaCivAgingStrategy();
    }

    /**
     * @return {@link UnitActionStrategy} for the setup
     */
    @Override
    public UnitActionStrategy createUnitActionStrategy() {
        return new GammaCivUnitActionStrategy();
    }

    /**
     * @return {@link UnitAttackStrategy} for the setup
     */
    @Override
    public UnitAttackStrategy createUnitAttackStrategy() {
        return new EpsilonUnitAttackStrategy(dieRollStrategy);
    }

    /**
     * @param game relevant game
     * @return {@link WinStrategy} for the setup
     */
    @Override
    public WinStrategy createWinStrategy(GameImpl game) {
        return new EpsilonCivWinStrategy(game);
    }

    /**
     * @return {@link WorldCreationStrategy} for the setup
     */
    @Override
    public WorldCreationStrategy createWorldCreationStrategy() {
        return new DeltaCivWorldCreationStrategy();
    }

    /**
     * @param game relevant game
     * @return {@link CityGrowthStrategy} for the setup
     */
    @Override
    public CityGrowthStrategy createCityGrowthStrategy(GameImpl game) {
        return new EtaCityGrowthStrategy();
    }

    /**
     * @return {@link CityResourcesStrategy} for the setup
     */
    @Override
    public CityResourcesStrategy createCityResourcesStrategy() {
        return new EtaCityResourcesStrategy();
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
