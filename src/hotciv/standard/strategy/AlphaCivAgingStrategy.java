package hotciv.standard.strategy;

import hotciv.framework.AgingStrategy;

public class AlphaCivAgingStrategy implements AgingStrategy {
    /**
     * Method that takes old age and returns new one.
     *
     * @param age the age from which the game is aging from
     * @return new age
     */
    @Override
    public int getNewAge(int age) {
        return age + 100;
    }
}
