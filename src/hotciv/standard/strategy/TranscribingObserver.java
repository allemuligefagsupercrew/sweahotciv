package hotciv.standard.strategy;

import hotciv.framework.*;

public class TranscribingObserver implements hotciv.framework.TranscribingObserver {
    private Game game;
    private boolean active;

    public TranscribingObserver(Game game) {
        game.addTranscribingObserver(this);
        active = true;
        this.game = game;
    }


    private void log(String loggingMessage) {
        if (active) {
            System.out.println(loggingMessage);
        }
    }

    /**
     * Activates the {@link hotciv.framework.TranscribingObserver}
     */
    @Override
    public void activate() {
        active = true;
    }

    /**
     * Activates the {@link hotciv.framework.TranscribingObserver}
     */
    @Override
    public void deactivate() {
        active = false;
    }

    /**
     * Notification about unit movement
     *
     * @param from position unit is moved from
     * @param to   position unit is moved to
     */
    @Override
    public void notifyUnitMove(Position from, Position to) {
        Unit unitAt = game.getUnitAt(to);
        log(unitAt.getOwner().toString() + " " + unitAt.getTypeString() + " moved from " + from + " to " + to);
    }

    /**
     * Notification about change in city ownership
     *
     * @param pos position of city
     */
    @Override
    public void notifyCityHasNewOwner(Position pos) {
        log(game.getCityAt(pos).getOwner().toString() + " took over city at position " + pos);
    }

    /**
     * Notification about creation of {@link Unit}
     *
     * @param pos position unit is spawned at
     */
    @Override
    public void notifyUnitCreation(Position pos) {
        Unit unitAt = game.getUnitAt(pos);
        log(unitAt.getOwner().toString() + " created " + unitAt.getTypeString() + " at position ");
    }

    /**
     * Notification on end of turn
     */
    @Override
    public void notifyEndOfTurn() {
        log(game.getPlayerInTurn().toString() + " ended their turn.");
    }

    /**
     * Notification upon update of winner
     */
    @Override
    public void notifyWinnerUpdated() {
        Player winner = game.getWinner();
        if (winner != null) {
            log(winner.toString() + " won the game!");
        }
    }

    /**
     * Notification upon change in city focus
     *
     * @param pos position of city
     */
    @Override
    public void notifyChangeOfFocus(Position pos) {
        City city = game.getCityAt(pos);
        log(city.getOwner().toString() + " changed focus in city at " + pos + " to " + city.getWorkforceFocus());
    }

    /**
     * Notification upon change in city production
     *
     * @param pos city position
     */
    @Override
    public void notifyProductionChange(Position pos) {
        City city = game.getCityAt(pos);
        log(city.getOwner().toString() + " changed production in city at " + pos + " to " + city.getProduction());
    }

    /**
     * Notification upon unit performing unitaction
     * <p>
     * Unit must be taken from parameter as unit can be removed upon action being performed
     *
     * @param pos    position of unit
     * @param unitAt unit performing action
     */
    @Override
    public void notifyUnitAction(Position pos, Unit unitAt) {
        log(unitAt.getOwner().toString() + " used unitaction on it's " + unitAt.getTypeString() + " on position " + pos);
    }
}
