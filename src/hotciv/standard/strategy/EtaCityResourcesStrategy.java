package hotciv.standard.strategy;

import hotciv.framework.CityResourcesStrategy;
import hotciv.framework.GameConstants;
import hotciv.framework.Position;
import hotciv.standard.CityImpl;
import hotciv.standard.GameImpl;
import hotciv.standard.RelativePositionEnum;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EtaCityResourcesStrategy implements CityResourcesStrategy {

    private final List<String> productionPriority = Arrays.asList(GameConstants.FOREST, GameConstants.HILLS, GameConstants.MOUNTAINS, GameConstants.PLAINS, GameConstants.OCEANS);
    private final List<String> foodPriority = Arrays.asList(GameConstants.PLAINS, GameConstants.OCEANS, GameConstants.FOREST, GameConstants.HILLS, GameConstants.MOUNTAINS);

    private Map<String, Integer> productionValuesMap;
    private Map<String, Integer> foodValuesMap;

    public EtaCityResourcesStrategy() {
        productionValuesMap = new HashMap<>();
        productionValuesMap.put(GameConstants.FOREST, 3);
        productionValuesMap.put(GameConstants.HILLS, 2);
        productionValuesMap.put(GameConstants.MOUNTAINS, 1);
        productionValuesMap.put(GameConstants.PLAINS, 0);
        productionValuesMap.put(GameConstants.OCEANS, 0);

        foodValuesMap = new HashMap<>();
        foodValuesMap.put(GameConstants.FOREST, 0);
        foodValuesMap.put(GameConstants.HILLS, 0);
        foodValuesMap.put(GameConstants.MOUNTAINS, 0);
        foodValuesMap.put(GameConstants.PLAINS, 3);
        foodValuesMap.put(GameConstants.OCEANS, 1);

    }

    /**
     * @param game     the game Object
     * @param position the position of the {@link hotciv.framework.City} in question
     * @return an integer representing the amount of food to be added to the city
     */
    @Override
    public int calculateFood(GameImpl game, Position position) {
        CityImpl city = (CityImpl) game.getCityAt(position);
        HashMap<String, Integer> tileTypeMap = getTileTypeMap(position, game);
        return getResourceValue(city, tileTypeMap, foodValuesMap);
    }

    /**
     * @param game     the game Object
     * @param position the position of the {@link hotciv.framework.City} in question
     * @return an integer representing the amount of food to be added to the city
     */
    @Override
    public int calculateProduction(GameImpl game, Position position) {
        CityImpl city = (CityImpl) game.getCityAt(position);
        HashMap<String, Integer> tileTypeMap = getTileTypeMap(position, game);
        return getResourceValue(city, tileTypeMap, productionValuesMap);
    }

    private int getResourceValue(CityImpl city, HashMap<String, Integer> tileTypeMap, Map<String, Integer> valueMap) {
        List<String> priority = getPriorityList(city);
        int value = 1;
        int remainingCitizens = city.getSize() - 1;
        for (String type : priority) {
            Integer nrOfTiles = tileTypeMap.get(type);
            if ((nrOfTiles != null) && (nrOfTiles >= remainingCitizens)) {
                return value + remainingCitizens * valueMap.get(type);
            } else if (nrOfTiles != null) {
                remainingCitizens -= nrOfTiles;
                value += nrOfTiles * valueMap.get(type);
            }
        }
        return value;
    }

    private List<String> getPriorityList(CityImpl city) {
        List<String> priority;
        if (GameConstants.productionFocus.equals(city.getWorkforceFocus())) {
            priority = productionPriority;
        } else {
            priority = foodPriority;
        }
        return priority;
    }


    private HashMap<String, Integer> getTileTypeMap(Position position, GameImpl game) {
        HashMap<String, Integer> tileTypeMap = new HashMap<>();
        for (RelativePositionEnum dir : RelativePositionEnum.values()) {
            String type = game.getTileAt(game.applyPositionOffset(position, dir)).getTypeString();
            tileTypeMap.merge(type, 1, (a, b) -> a + b);
        }
        return tileTypeMap;
    }
}
