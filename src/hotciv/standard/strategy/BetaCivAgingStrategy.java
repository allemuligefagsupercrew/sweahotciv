package hotciv.standard.strategy;

import hotciv.framework.AgingStrategy;

public class BetaCivAgingStrategy implements AgingStrategy {
    /**
     * Method that takes old age and returns new one.
     *
     * @param age the age from which the game is aging from
     * @return new age
     */
    @Override
    public int getNewAge(int age) {
        if (age < -100) {
            return age + 100;
        } else if (age == -100) {
            return -1;
        } else if (age == -1) {
            return 1;
        } else if (age == 1) {
            return 50;
        } else if (age < 1750) {
            return age + 50;
        } else if (age < 1900) {
            return age + 25;
        } else if (age < 1970) {
            return age + 5;
        } else {
            return age + 1;
        }
    }
}
