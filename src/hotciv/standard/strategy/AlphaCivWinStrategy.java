package hotciv.standard.strategy;

import hotciv.framework.Player;
import hotciv.framework.WinStrategy;
import hotciv.standard.GameImpl;

public class AlphaCivWinStrategy implements WinStrategy {
    /**
     * Method run when strategy is activated.
     * Takes game and city as parameters. This is so the strategy knows what city has been captured, and are able to read or modify the game.
     *
     * @param game - the instance of the game being played.
     */
    @Override
    public Player getWinner(GameImpl game) {
        if (game.getAge() == -3000) {
            return Player.RED;
        }
        return null;
    }
}
