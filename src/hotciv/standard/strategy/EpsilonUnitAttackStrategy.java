package hotciv.standard.strategy;

import hotciv.framework.DieRollStrategy;
import hotciv.framework.GameConstants;
import hotciv.framework.Position;
import hotciv.framework.UnitAttackStrategy;
import hotciv.standard.GameImpl;
import hotciv.standard.RelativePositionEnum;

/**
 * Created by Johannes on 01-10-2017.
 */
public class EpsilonUnitAttackStrategy implements UnitAttackStrategy {
    private DieRollStrategy rollStrategy;
    private GameImpl game;

    public EpsilonUnitAttackStrategy(DieRollStrategy rollStrategy) {
        this.rollStrategy = rollStrategy;
    }

    @Override
    public boolean getOutcome(GameImpl game, Position attackerPos, Position defendantPos) {
        this.game = game;
        int d1 = rollStrategy.rollDie();
        int d2 = rollStrategy.rollDie();
        return getAttackStrength(attackerPos) * d1 > getDefensiveStrength(defendantPos) * d2;
    }


    private int getDefensiveStrength(Position pos) {
        int defensiveStrength = game.getUnitAt(pos).getDefensiveStrength() + getNumberOfNearbyFriendlies(pos);
        return getTileMultiplier(pos, defensiveStrength);
    }

    private int getAttackStrength(Position pos) {
        int baseAttack = game.getUnitAt(pos).getAttackingStrength() + getNumberOfNearbyFriendlies(pos);
        return getTileMultiplier(pos, baseAttack);
    }

    private int getTileMultiplier(Position pos, int baseValue) {
        if (game.getCityAt(pos) != null) {
            return 3 * baseValue;
        } else if (GameConstants.FOREST.equals(game.getTileAt(pos).getTypeString()) ||
                GameConstants.HILLS.equals(game.getTileAt(pos).getTypeString())) {
            return 2 * baseValue;
        } else {
            return baseValue;
        }
    }

    private int getNumberOfNearbyFriendlies(Position pos) {
        int cnt = 0;
        for (RelativePositionEnum relPos : RelativePositionEnum.values()) {
            Position adjPos = game.applyPositionOffset(pos, relPos);
            if (game.getUnitAt(adjPos) != null && game.getPlayerInTurn().equals(game.getUnitAt(adjPos).getOwner())) {
                cnt++;
            }
        }
        return cnt;
    }


}
