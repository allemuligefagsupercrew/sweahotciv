package hotciv.standard.strategy;

import hotciv.framework.Position;
import hotciv.framework.UnitAttackStrategy;
import hotciv.standard.GameImpl;

/**
 * Created by Johannes on 01-10-2017.
 */
public class AlphaCivUnitAttackStrategy implements UnitAttackStrategy {
    @Override
    public boolean getOutcome(GameImpl game, Position attackerPos, Position defendantPos) {
        return true;
    }
}
