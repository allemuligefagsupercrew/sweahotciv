package hotciv.standard.strategy;

import hotciv.framework.*;
import hotciv.standard.GameImpl;
import hotciv.standard.UnitImpl;
import thirdparty.ThirdPartyFractalGenerator;

import java.util.HashMap;
import java.util.Map;

public class RandomWorldGenerationStrategy implements WorldCreationStrategy {
    public static void main(String[] args) {

        GameImpl game = new GameImpl(new TestFactory());

        String line;
        for (int r = 0; r < GameConstants.WORLDSIZE; r++) {
            line = "";
            for (int c = 0; c < GameConstants.WORLDSIZE; c++) {
                line += game.getTileAt(new Position(r, c)).getTypeString() + " ";
            }
            System.out.println(line);
        }
    }

    /**
     * @return stringarray representing a world-layout
     */
    @Override
    public String[] getWorldLayout() {
        ThirdPartyFractalGenerator generator = new ThirdPartyFractalGenerator();

        String line;
        String[] world = new String[GameConstants.WORLDSIZE];
        for (int r = 0; r < GameConstants.WORLDSIZE; r++) {
            line = "";
            for (int c = 0; c < GameConstants.WORLDSIZE; c++) {
                line += generator.getLandscapeAt(r, c);
            }
            world[r] = line;
        }
        return world;
    }

    /**
     * @return the cityMap for the initial game setup
     */
    @Override
    public Map<Position, City> getCityMap() {
        return new HashMap<>(); //TODO: Fix this so we don't have to return HashMap.
    }

    /**
     * @return the map between objects of type {@link Position} and {@link UnitImpl} describing the position of the units
     */
    @Override
    public Map<Position, UnitImpl> getUnitMap() {
        return new HashMap<>(); //TODO: Fix this so we don't have to return HashMap.
    }


    private static class TestFactory implements Factory {

        /**
         * @return {@link AgingStrategy} for the setup
         */
        @Override
        public AgingStrategy createAgingStrategy() {
            return new AlphaCivAgingStrategy();
        }

        /**
         * @return {@link UnitActionStrategy} for the setup
         */
        @Override
        public UnitActionStrategy createUnitActionStrategy() {
            return new VoidUnitActionStrategy();
        }

        /**
         * @return {@link UnitAttackStrategy} for the setup
         */
        @Override
        public UnitAttackStrategy createUnitAttackStrategy() {
            return new AlphaCivUnitAttackStrategy();
        }

        /**
         * @param game
         * @return {@link WinStrategy} for the setup
         */
        @Override
        public WinStrategy createWinStrategy(GameImpl game) {
            return new AlphaCivWinStrategy();
        }

        /**
         * @return {@link WorldCreationStrategy} for the setup
         */
        @Override
        public WorldCreationStrategy createWorldCreationStrategy() {
            return new RandomWorldGenerationStrategy();
        }

        /**
         * @param game relevant game
         * @return {@link CityGrowthStrategy} for the setup
         */
        @Override
        public CityGrowthStrategy createCityGrowthStrategy(GameImpl game) {
            return new AlphaCityGrowthStrategy();
        }

        /**
         * @return {@link CityResourcesStrategy} for the setup
         */
        @Override
        public CityResourcesStrategy createCityResourcesStrategy() {
            return new AlphaCivResourcesStrategy();
        }
    }

}
