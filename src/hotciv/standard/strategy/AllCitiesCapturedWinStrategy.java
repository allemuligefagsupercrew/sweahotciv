package hotciv.standard.strategy;

import hotciv.framework.City;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.framework.WinStrategy;
import hotciv.standard.GameImpl;

import java.util.Map;

public class AllCitiesCapturedWinStrategy implements WinStrategy {
    /**
     * Method run when strategy is activated.
     * Takes game and city as parameters. This is so the strategy knows what city has been captured, and are able to read or modify the game.
     *
     * @param game - the instance of the game being played.
     */
    @Override
    public Player getWinner(GameImpl game) {
        Map<Position, City> cityMap = game.getCityMap();
        Player owner = cityMap.get(cityMap.keySet().iterator().next()).getOwner();

        for (Position pos : cityMap.keySet()) {
            if (!owner.equals(cityMap.get(pos).getOwner())) {
                return null;
            }
        }
        return owner;
    }
}
