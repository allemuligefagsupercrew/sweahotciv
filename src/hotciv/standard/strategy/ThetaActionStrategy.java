package hotciv.standard.strategy;

import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.framework.UnitActionStrategy;
import hotciv.standard.GameImpl;
import hotciv.standard.UnitImpl;

public class ThetaActionStrategy implements UnitActionStrategy {
    @Override
    public void performUnitActionAt(GameImpl game, Position p) {
        if (GameConstants.GALLEY.equals(game.getUnitAt(p).getTypeString())) {
            Player owner = game.getUnitAt(p).getOwner();
            UnitImpl unit = new UnitImpl(GameConstants.SETTLER, owner);
            Position position = game.firstFreeTile(p, unit);
            if (position == null) return;
            game.createUnit(position, unit);
            game.removeUnit(p);
        }
    }

}
