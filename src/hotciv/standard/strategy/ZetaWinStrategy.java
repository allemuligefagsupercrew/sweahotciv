package hotciv.standard.strategy;

import hotciv.framework.AttackObserver;
import hotciv.framework.Player;
import hotciv.framework.WinStrategy;
import hotciv.standard.GameImpl;

public class ZetaWinStrategy implements WinStrategy, AttackObserver {
    private WinStrategy state = new AllCitiesCapturedWinStrategy();

    public ZetaWinStrategy(GameImpl game) {
        game.addAttackObserver(this);
    }

    /**
     * This implementation relies on the invariant that it's called once every turn. This could potentially be dangerous.
     *
     * @param game - the instance of the game being played.
     * @return the winner of the game
     */
    @Override
    public Player getWinner(GameImpl game) {
        if (game.getRound() == 20) {
            state = new EpsilonCivWinStrategy(game);
        }
        return state.getWinner(game);
    }

    /**
     * Notifies observer about an attack.
     *
     * @param player - the player who is the subject of the notification
     */
    @Override
    public void notifyObserver(Player player) {
        if (state instanceof EpsilonCivWinStrategy) {
            ((EpsilonCivWinStrategy) state).notifyObserver(player);
        }
    }
}
