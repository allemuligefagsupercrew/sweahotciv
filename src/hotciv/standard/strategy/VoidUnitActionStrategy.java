package hotciv.standard.strategy;

import hotciv.framework.Position;
import hotciv.framework.UnitActionStrategy;
import hotciv.standard.GameImpl;

public class VoidUnitActionStrategy implements UnitActionStrategy {
    @Override
    public void performUnitActionAt(GameImpl game, Position p) {

    }
}
