package hotciv.standard.strategy;

import hotciv.framework.CityGrowthStrategy;
import hotciv.standard.CityImpl;

public class EtaCityGrowthStrategy implements CityGrowthStrategy {
    /**
     * Handles growth of the city
     *
     * @param city the relevant city
     */
    @Override
    public void handleGrowth(CityImpl city) {
        int size = city.getSize();
        int food = city.getFood();
        if (canGrow(size, food) && allowedToGrow(city)) {
            city.grow();
        }
    }

    private boolean allowedToGrow(CityImpl city) {
        return city.getSize() < 9;
    }

    private boolean canGrow(int size, int food) {
        return 5 + size * 3 <= food;
    }
}
