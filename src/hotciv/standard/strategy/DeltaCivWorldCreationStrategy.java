package hotciv.standard.strategy;

import hotciv.framework.*;
import hotciv.standard.CityImpl;
import hotciv.standard.UnitImpl;

import java.util.HashMap;
import java.util.Map;

import static java.lang.Math.pow;

public class DeltaCivWorldCreationStrategy implements WorldCreationStrategy {
    /**
     * @return stringarray representing a world-layout
     */
    @Override
    public String[] getWorldLayout() {
        return new String[]{
                "...ooMooooo.....",
                "..ohhoooofffoo..",
                ".oooooMooo...oo.",
                ".ooMMMoooo..oooo",
                "...ofooohhoooo..",
                ".ofoofooooohhoo.",
                "...ooo..........",
                ".ooooo.ooohooM..",
                ".ooooo.oohooof..",
                "offfoooo.offoooo",
                "oooooooo...ooooo",
                ".ooMMMoooo......",
                "..ooooooffoooo..",
                "....ooooooooo...",
                "..ooohhoo.......",
                ".....ooooooooo..",
        };
    }

    /**
     * @return the cityMap for the initial game setup
     */
    @Override
    public Map<Position, City> getCityMap() {
        HashMap<Position, City> cityMap = new HashMap<>();
        cityMap.put(new Position(8, 12), new CityImpl(Player.RED));
        cityMap.put(new Position(4, 5), new CityImpl(Player.BLUE));
        return cityMap;
    }

    /**
     * @return the map between objects of type {@link Position} and {@link UnitImpl} describing the position of the units
     */
    @Override
    public Map<Position, UnitImpl> getUnitMap() {
        HashMap<Position, UnitImpl> units = new HashMap<Position, UnitImpl>((int) pow(GameConstants.WORLDSIZE, 2));
        units.put(new Position(4, 4), new UnitImpl(GameConstants.LEGION, Player.BLUE));
        units.put(new Position(3, 8), new UnitImpl(GameConstants.ARCHER, Player.RED));
        units.put(new Position(5, 5), new UnitImpl(GameConstants.SETTLER, Player.RED));
        return units;
    }
}
