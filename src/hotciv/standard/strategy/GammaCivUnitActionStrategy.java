package hotciv.standard.strategy;

import hotciv.framework.*;
import hotciv.standard.CityImpl;
import hotciv.standard.GameImpl;

import java.util.Map;

public class GammaCivUnitActionStrategy implements UnitActionStrategy {

    @Override
    public void performUnitActionAt(GameImpl game, Position p) {
        Unit unit = game.getUnitAt(p);
        Map<Position, City> cityMap = game.getCityMap();

        if (unit.getTypeString().equals(GameConstants.SETTLER) &&
                game.getPlayerInTurn().equals(unit.getOwner())) {

            game.removeUnit(p);
            cityMap.put(p, new CityImpl(game.getPlayerInTurn()));

        } else if (unit.getTypeString().equals(GameConstants.ARCHER) &&
                game.getPlayerInTurn().equals(unit.getOwner())) {
            if (unit.getDefensiveStrength() == 6) {
                unit.setDefensiveStrength(3);
            } else {
                unit.setDefensiveStrength(6);
            }
        }
    }

}
