package hotciv.standard.strategy;

import hotciv.framework.AttackObserver;
import hotciv.framework.Player;
import hotciv.framework.WinStrategy;
import hotciv.standard.GameImpl;

import java.util.HashMap;
import java.util.Map;

public class EpsilonCivWinStrategy implements WinStrategy, AttackObserver {
    private Map<Player, Integer> winCount = new HashMap<>();

    public EpsilonCivWinStrategy(GameImpl game) {
        game.addAttackObserver(this);
        for (Player player : Player.values()) {
            winCount.put(player, 0);
        }

    }

    @Override
    public Player getWinner(GameImpl game) {
        for (Player player : winCount.keySet()) {
            if (winCount.get(player) >= 3) {
                return player;
            }
        }
        return null;
    }

    /**
     * Notifies observer about an attack.
     *
     * @param player - the player who is the subject of the notification
     */
    @Override
    public void notifyObserver(Player player) {
        winCount.put(player, winCount.get(player) + 1);
    }
}