package hotciv.standard.strategy;

import hotciv.framework.DieRollStrategy;

import java.util.Random;

public class RandomDieRoll implements DieRollStrategy {
    /**
     * Interface used when imitating rolling a die
     *
     * @return integer value from dieroll
     */
    @Override
    public int rollDie() {
        Random random = new Random();
        return random.nextInt(6) + 1;
    }
}
