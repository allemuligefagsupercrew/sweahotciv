package hotciv.standard.strategy;

import hotciv.framework.City;
import hotciv.framework.CityResourcesStrategy;
import hotciv.framework.Position;
import hotciv.standard.GameImpl;

public class AlphaCivResourcesStrategy implements CityResourcesStrategy {
    /**
     * @param game     the game Object
     * @param position the position of the {@link City} in question
     * @return an integer representing the amount of food to be added to the city
     */
    @Override
    public int calculateFood(GameImpl game, Position position) {
        return 0;
    }

    /**
     * @param game     the game Object
     * @param position the position of the {@link City} in question
     * @return an integer representing the amount of food to be added to the city
     */
    @Override
    public int calculateProduction(GameImpl game, Position position) {
        return 6;
    }
}
