package hotciv.standard;

import hotciv.framework.Tile;

public class TileImpl implements Tile {

    private String type;

    public TileImpl(String type) {
        this.type = type;
    }

    /**
     * return the tile type as a string. The set of
     * valid strings are defined by the graphics
     * engine, as they correspond to named image files.
     *
     * @return the type type as string
     */
    @Override
    public String getTypeString() {
        return type;
    }
}
