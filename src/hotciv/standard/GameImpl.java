package hotciv.standard;

import hotciv.framework.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Math.abs;
import static java.lang.Math.max;

public class GameImpl implements Game {
    private WorldCreationStrategy worldCreationStrategy;
    private Player player;
    private int age;
    private Map<Position, Tile> worldTiles;
    private Map<Position, UnitImpl> worldUnits;
    private Map<Position, City> cityMap;
    private int round = 0;

    private Player winner;
    private WinStrategy winStrategy;
    private UnitAttackStrategy unitAttackStrategy;
    private UnitActionStrategy unitActionStrategy;
    private CityGrowthStrategy cityGrowthStrategy;
    private CityResourcesStrategy cityResourcesStrategy;
    private AgingStrategy agingStrategy;
    private List<AttackObserver> attackObservers;
    private List<TranscribingObserver> transcribingObservers;
    private ArrayList<GameObserver> gameObservers;

    public GameImpl(Factory factory) {

        attackObservers = new ArrayList<>();
        transcribingObservers = new ArrayList<>();
        gameObservers = new ArrayList<>();

        initializeStrategies(factory);

        worldUnits = worldCreationStrategy.getUnitMap();

        cityMap = worldCreationStrategy.getCityMap();
        worldTiles = parseWorld(worldCreationStrategy.getWorldLayout());


        age = -4000;
        this.player = Player.RED;
    }

    /**
     * Adds and {@link TranscribingObserver} to the list of observers in the game
     *
     * @param transcribingObserver the observer to be notified on events
     */
    @Override
    public void addTranscribingObserver(TranscribingObserver transcribingObserver) {
        transcribingObservers.add(transcribingObserver);
    }

    /**
     * Activates all connected listeners of type {@link TranscribingObserver}
     */
    @Override
    public void activateTranscribingListeners() {
        for (TranscribingObserver observer : transcribingObservers) {
            observer.activate();
        }
    }

    /**
     * Deactivates all connected listeners of type {@link TranscribingObserver}
     */
    @Override
    public void deactivateTranscribingListeners() {
        for (TranscribingObserver observer : transcribingObservers) {
            observer.deactivate();
        }
    }

    private void initializeStrategies(Factory factory) {
        agingStrategy = factory.createAgingStrategy();
        unitAttackStrategy = factory.createUnitAttackStrategy();
        unitActionStrategy = factory.createUnitActionStrategy();
        worldCreationStrategy = factory.createWorldCreationStrategy();
        winStrategy = factory.createWinStrategy(this);
        cityGrowthStrategy = factory.createCityGrowthStrategy(this);
        cityResourcesStrategy = factory.createCityResourcesStrategy();
    }

    @Override
    public Tile getTileAt(Position p) {
        return worldTiles.get(p);
    }

    @Override
    public Unit getUnitAt(Position p) {
        return worldUnits.get(p);
    }

    @Override
    public City getCityAt(Position p) {
        return cityMap.get(p);
    }

    @Override
    public Player getPlayerInTurn() {
        return player;
    }

    @Override
    public Player getWinner() {
        return winner;
    }

    @Override
    public int getAge() {
        return age;
    }

    public int getRound() {
        return round;
    }

    @Override
    public boolean moveUnit(Position from, Position to) {
        if (!canMove(from, to)) return false;
        if (hasEnemy(to)) {
            if (!unitAttackStrategy.getOutcome(this, from, to)) {
                worldUnits.remove(from);
                for (GameObserver gameObserver : gameObservers) {
                    gameObserver.worldChangedAt(from);
                }
                return false;
            }
            notifyAttackingObservers();
        }
        updateUnitPlacementAndCityOwner(from, to);
        return true;
    }

    private void notifyAttackingObservers() {
        for (AttackObserver observer : attackObservers) {
            observer.notifyObserver(player);
        }
    }

    private boolean hasEnemy(Position pos) {
        UnitImpl unit = worldUnits.get(pos);
        return unit != null && !player.equals(unit.getOwner());
    }

    /**
     * Adds an {@link AttackObserver} to the list of observers in the game.
     *
     * @param attackObserver the {@link AttackObserver} that should be notified on attacks,
     */
    @Override
    public void addAttackObserver(AttackObserver attackObserver) {
        attackObservers.add(attackObserver);
    }


    private void updateUnitPlacementAndCityOwner(Position from, Position to) {
        updateUnitPlacement(from, to);
        updateCityOwner(to);
        for (GameObserver gameObserver : gameObservers) {
            gameObserver.worldChangedAt(from);
            gameObserver.worldChangedAt(to);
        }
    }

    private void updateCityOwner(Position pos) {
        City city = getCityAt(pos);
        if (city != null && !player.equals(city.getOwner())) {
            for (TranscribingObserver observer : transcribingObservers) {
                observer.notifyCityHasNewOwner(pos);
            }
            takeOverCity(city);
        }
    }

    private void takeOverCity(City city) {
        city.setOwner(player);
    }

    private void updateUnitPlacement(Position from, Position to) {
        UnitImpl unit = worldUnits.remove(from);
        unit.useMoves(distance(from, to));
        worldUnits.put(to, unit);
        for (TranscribingObserver observer : transcribingObservers) {
            observer.notifyUnitMove(from, to);
        }
    }

    private boolean canMove(Position from, Position to) {
        Unit unitAt = getUnitAt(from);
        if (unitAt.isFortified() || isOutOfMoves(unitAt) || isPlayerInTurnDifferentFromOwner(unitAt) || isUnitOutOfRange(from, to)) {
            return false;
        }
        return isLegalTile(to, unitAt);

    }

    private boolean isUnitOutOfRange(Position from, Position to) {
        return distance(from, to) > getUnitAt(from).getMoveCount();
    }

    private int distance(Position p1, Position p2) {
        return max(abs(p1.getRow() - p2.getRow()), abs(p1.getColumn() - p2.getColumn()));
    }

    private boolean isPlayerInTurnDifferentFromOwner(Unit unitAt) {
        return !unitAt.getOwner().equals(player);
    }

    private boolean isOutOfMoves(Unit unitAt) {
        return unitAt.getMoveCount() != 1;
    }

    private boolean isLegalTile(Position to, Unit unit) {
        String tileType = getTileAt(to).getTypeString();
        boolean illegalType = !unit.getCompatibleTileTypes().contains(tileType);
        if (illegalType) return false;

        boolean containsFriendlyUnit = worldUnits.containsKey(to) && worldUnits.get(to).getOwner().equals(unit.getOwner());
        if (containsFriendlyUnit) return false;

        return true;

    }

    public Map<Position, City> getCityMap() {
        return cityMap;
    }

    public void removeUnit(Position p) {
        worldUnits.remove(p);
    }

    /**
     * This method is ONLY to be used by ActionStrategies - NOT a user interface
     *
     * @param pos  position that the unit should be placed in
     * @param unit the unit object that should be placed
     */
    public void createUnit(Position pos, UnitImpl unit) {
        for (TranscribingObserver observer : transcribingObservers) {
            observer.notifyUnitCreation(pos);
        }
        for (GameObserver gameObserver : gameObservers) {
            gameObserver.worldChangedAt(pos);
        }
        worldUnits.put(pos, unit);
    }

    public Position firstFreeTile(Position initPosition, Unit unit) {
        if (isLegalTile(initPosition, unit) && !containsUnit(initPosition)) {
            return initPosition;
        } else {
            for (RelativePositionEnum direction : RelativePositionEnum.values()) {
                Position newPosition = applyPositionOffset(initPosition, direction);
                if (isLegalTile(newPosition, unit) && !containsUnit(newPosition)) {
                    return newPosition;
                }
            }
        }
        return null;
    }

    private boolean containsUnit(Position pos) {
        return getUnitAt(pos) != null;
    }

    public Position applyPositionOffset(Position pos, RelativePositionEnum direction) {
        Position directionPosition = RelativePositionEnum.getOffset(direction);
        return new Position(pos.getRow() + directionPosition.getRow(),
                pos.getColumn() + directionPosition.getColumn());
    }


    @Override
    public void endOfTurn() {
        for (TranscribingObserver observer : transcribingObservers) {
            observer.notifyEndOfTurn();
        }
        if (Player.RED.equals(player)) {
            player = Player.BLUE;
        } else {
            endOfRound();
            incRound();
            player = Player.RED;

        }
        for (GameObserver gameObserver : gameObservers) {
            gameObserver.turnEnds(player, age);
        }
        updateWinner();
        resetMoveCount();
    }

    private void updateWinner() {
        if (winner == null) {
            winner = winStrategy.getWinner(this);
            for (TranscribingObserver observer : transcribingObservers) {
                observer.notifyWinnerUpdated();
            }
        }
    }

    private void endOfRound() {
        performEndOfTurnForCities();
        updateGameAge();
    }

    private void performEndOfTurnForCities() {
        for (Position position : cityMap.keySet()) {
            CityImpl city = (CityImpl) getCityAt(position);
            cityEndOfTurn(position, city);
        }
    }

    private void cityEndOfTurn(Position position, CityImpl city) {
        city.addProduction(cityResourcesStrategy.calculateProduction(this, position));
        city.addFood(cityResourcesStrategy.calculateFood(this, position));
        cityGrowthStrategy.handleGrowth(city);
        produceUnits(position);
    }


    private void updateGameAge() {
        age = agingStrategy.getNewAge(age);
    }


    private void resetMoveCount() {
        for (Position position : worldUnits.keySet()) {
            worldUnits.get(position).resetMoveCount();
        }
    }

    private void produceUnits(Position p) {
        City city = cityMap.get(p);
        if (city.hasEnoughProduction()) {
            UnitImpl unit = city.retrieveProducedUnit();
            Position firstFreeTile = firstFreeTile(p, unit);
            worldUnits.put(firstFreeTile, unit);
            for (GameObserver gameObserver : gameObservers) {
                gameObserver.worldChangedAt(firstFreeTile);
            }
        }
    }

    private void incRound() {
        round++;
    }


    @Override
    public void changeWorkForceFocusInCityAt(Position pos, String balance) {
        getCityAt(pos).setWorkforceFocus(balance);
        for (TranscribingObserver observer : transcribingObservers) {
            observer.notifyChangeOfFocus(pos);
        }
        for (GameObserver gameObserver : gameObservers) {
            gameObserver.worldChangedAt(pos);
        }
    }

    @Override
    public void changeProductionInCityAt(Position pos, String unitType) {
        if (player.equals(getCityAt(pos).getOwner())) {
            if (GameConstants.GALLEY.equals(unitType)) {
                if (!isWaterNearby(pos)) return;
            }
            getCityAt(pos).setProduction(unitType);
            for (TranscribingObserver observer : transcribingObservers) {
                observer.notifyProductionChange(pos);
            }
        }
    }

    private boolean isWaterNearby(Position p) {
        for (RelativePositionEnum dir : RelativePositionEnum.values()) {
            String tileType = getTileAt(applyPositionOffset(p, dir)).getTypeString();
            if (tileType.equals(GameConstants.OCEANS)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void performUnitActionAt(Position pos) {
        Unit unitAt = getUnitAt(pos);
        if (unitAt.getOwner().equals(getPlayerInTurn())) {
            unitActionStrategy.performUnitActionAt(this, pos);
            for (TranscribingObserver observer : transcribingObservers) {
                observer.notifyUnitAction(pos, unitAt);
            }
            for (GameObserver gameObserver : gameObservers) {
                gameObserver.worldChangedAt(pos);
            }
        }
    }

    private Map<Position, Tile> parseWorld(String[] layout) {
        Map<Position, Tile> theWorld = new HashMap<Position, Tile>();
        for (int r = 0; r < GameConstants.WORLDSIZE; r++) {
            String line = layout[r];
            for (int c = 0; c < GameConstants.WORLDSIZE; c++) {
                char tileChar = line.charAt(c);
                theWorld.put(new Position(r, c), getTileFromChar(tileChar));
            }
        }
        return theWorld;

    }

    private TileImpl getTileFromChar(char tileChar) {
        String type = getTileType(tileChar);
        return new TileImpl(type);
    }

    private String getTileType(char tileChar) {
        switch (tileChar) {
            case '.':
                return GameConstants.OCEANS;
            case 'o':
                return GameConstants.PLAINS;
            case 'M':
                return GameConstants.MOUNTAINS;
            case 'f':
                return GameConstants.FOREST;
            case 'h':
                return GameConstants.HILLS;
            default:
                throw new IllegalArgumentException("No such tiletype");
        }
    }

    @Override
    public void setTileFocus(Position position) {
        for (GameObserver gameObserver : gameObservers) {
            gameObserver.tileFocusChangedAt(position);
        }
    }

    @Override
    public void addObserver(GameObserver gameObserver) {
        gameObservers.add(gameObserver);

    }
}
