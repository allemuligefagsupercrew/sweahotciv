package hotciv.standard;

import hotciv.framework.*;


public class    CityImpl implements City {
    private Player player;
    private int production = 0;
    private String unitInProduction;
    private int food;
    private int citySize;
    private String workForceFocus;


    public CityImpl(Player player) {
        this.player = player;
        workForceFocus = GameConstants.productionFocus;
        citySize = 1;

    }

    @Override
    public Player getOwner() {
        return player;
    }

    public void setOwner(Player owner) {
        this.player = owner;
    }

    @Override
    public int getSize() {
        return citySize;
    }

    @Override
    public int getAvailableProduction() {
        return production;
    }

    @Override
    public String getProduction() {
        return unitInProduction;
    }

    @Override
    public void setProduction(String unit) {
        unitInProduction = unit;
    }

    void addProduction(int production) {
        this.production += production;
    }

    void addFood(int food) {
        this.food += food;
    }

    /**
     * @return Whether the city has finished producing the unit selected for production
     */
    @Override
    public boolean hasEnoughProduction() {
        if (unitInProduction == null) {
            return false;
        }

        return getUnitPrice(unitInProduction) <= production;
    }

    private int getUnitPrice(String unitType) {
        switch (unitType) {
            case GameConstants.ARCHER:
                return GameConstants.ARCHER_PRICE;
            case GameConstants.LEGION:
                return GameConstants.LEGION_PRICE;
            case GameConstants.SETTLER:
                return GameConstants.SETTLER_PRICE;
            case GameConstants.GALLEY:
                return GameConstants.GALLEY_PRICE;
            default:
                throw new RuntimeException("Unittype " + unitType + "Not mapped to a price");

        }
    }

    @Override
    public UnitImpl retrieveProducedUnit() {
        if (hasEnoughProduction()) {
            production -= getUnitPrice(unitInProduction);
            return new UnitImpl(unitInProduction, player);
        }
        return null;
    }

    @Override
    public String getWorkforceFocus() {
        return workForceFocus;
    }

    /**
     * @param balance the new workforce focus
     */
    @Override
    public void setWorkforceFocus(String balance) {
        workForceFocus = balance;
    }

    public int getFood() {
        return food;
    }

    /**
     * Makes the City Grow
     * <p>
     * Should ONLY be used by {@link CityGrowthStrategy}
     */
    @Override
    public void grow() {
        food = 0;
        citySize += 1;
    }
}
