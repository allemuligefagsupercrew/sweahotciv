package hotciv.standard;

import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Unit;

import java.util.ArrayList;
import java.util.List;

public class UnitImpl implements Unit {
    private final int standardMoveCount;
    private String type;
    private Player owner;
    private int moves;
    private int defensiveStrength;
    private int attackingStrength;
    private boolean fortified = false;

    public UnitImpl(String type, Player owner) {
        this.type = type;
        this.owner = owner;


        setFieldValues(type);
        moves = 1;
        standardMoveCount = type.equals(GameConstants.GALLEY) ? 2 : 1;
    }

    @Override
    public List<String> getCompatibleTileTypes() {
        ArrayList<String> types = new ArrayList<>();
        switch (type) {
            case GameConstants.ARCHER:
            case GameConstants.SETTLER:
            case GameConstants.LEGION:
                types.add(GameConstants.HILLS);
                types.add(GameConstants.FOREST);
                types.add(GameConstants.PLAINS);
                break;
            case GameConstants.GALLEY:
                types.add(GameConstants.OCEANS);
        }
        return types;

    }

    private void setFieldValues(String type) {
        switch (type) {
            case GameConstants.ARCHER:
                attackingStrength = 2;
                defensiveStrength = 3;
                break;
            case GameConstants.LEGION:
                defensiveStrength = 2;
                attackingStrength = 4;
                break;
            case GameConstants.SETTLER:
                defensiveStrength = 3;
                attackingStrength = 0;
                break;
            case GameConstants.GALLEY:
                defensiveStrength = 2;
                attackingStrength = 8;
                break;
            default:
                throw new RuntimeException(type + " is not a recognized type in HotCiv");
        }

    }

    /**
     * return the type of the unit
     *
     * @return unit type as a string, valid values are at
     * least those listed in GameConstants, particular variants
     * may define more strings to be valid.
     */
    @Override
    public String getTypeString() {
        return type;
    }

    /**
     * return the owner of this unit.
     *
     * @return the player that controls this unit.
     */
    @Override
    public Player getOwner() {
        return owner;
    }

    /**
     * return the move distance left (move count).
     * A move count of N means the unit may travel
     * a distance of N in this turn. The move count
     * is reset to the units maximal value before
     * a new turn starts.
     *
     * @return the move count
     */
    @Override
    public int getMoveCount() {
        return moves;
    }

    /**
     * Reduces the units movepoints - to be used when moving a unit
     *
     * @param moves the amount of moves to be used
     */
    public void useMoves(int moves) {
        this.moves -= moves;
    }

    /**
     * Resets the number of moves, so that the unit can move again.
     */
    protected void resetMoveCount() {
        moves = standardMoveCount;
    }

    /**
     * return the defensive strength of this unit
     *
     * @return defensive strength
     */
    @Override
    public int getDefensiveStrength() {
        return defensiveStrength;
    }

    /**
     * Sets new DefensiveStrength strength
     */
    @Override
    public void setDefensiveStrength(int def) {
        defensiveStrength = def;
    }

    /**
     * return the attack strength of this unit
     *
     * @return attack strength
     */
    @Override
    public int getAttackingStrength() {
        return attackingStrength;
    }

    @Override
    public boolean isFortified() {
        return defensiveStrength > 3;
    }

}
