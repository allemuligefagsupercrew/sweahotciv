package hotciv.standard;

import hotciv.framework.GameObserver;
import hotciv.framework.Player;
import hotciv.framework.Position;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GameObserverTestSpy implements GameObserver {
    private Set<Position> changeSet;
    private List<EndOfTurnObject> endOfTurnObjects;

    public GameObserverTestSpy() {
        changeSet = new HashSet<>();
        endOfTurnObjects = new ArrayList<>();
    }

    public Set<Position> getChangeSet() {
        return changeSet;
    }

    public List<EndOfTurnObject> getEndOfTurnObjects() {
        return endOfTurnObjects;
    }

    /**
     * invoked every time some change occurs on a position
     * in the world - a unit disappears or appears, a
     * city appears, a city changes player color, or any
     * other event that requires the GUI to redraw the
     * graphics on a particular position.
     *
     * @param pos the position in the world that has changed state
     */
    @Override
    public void worldChangedAt(Position pos) {
        changeSet.add(pos);
    }

    /**
     * invoked just after the game's end of turn is called
     * to signal the new "player in turn" and world age state.
     *
     * @param nextPlayer the next player that may move units etc.
     * @param age        the present age of the world
     */
    @Override
    public void turnEnds(Player nextPlayer, int age) {
        endOfTurnObjects.add(new EndOfTurnObject(nextPlayer, age));
    }

    /**
     * invoked whenever the user changes focus to another
     * tile (for inspecting the tile's unit and city
     * properties.)
     *
     * @param position the position of the tile that is
     *                 now inspected/has focus.
     */
    @Override
    public void tileFocusChangedAt(Position position) {

    }

    public class EndOfTurnObject {
        private Player nextPlayer;
        private int age;

        public EndOfTurnObject(Player nextPlayer, int age) {
            this.nextPlayer = nextPlayer;
            this.age = age;
        }

        public Player getNextPlayer() {
            return nextPlayer;
        }

        public int getAge() {
            return age;
        }
    }
}
