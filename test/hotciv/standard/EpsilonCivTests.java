package hotciv.standard;

import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Position;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * Created by Johannes on 01-10-2017.
 */
public class EpsilonCivTests extends EpsilonCivInit {

    @Test
    public void shouldFailAttackAndDestroySettler() {
        game.moveUnit(new Position(4, 3), new Position(3, 2));
        assertNull(game.getUnitAt(new Position(4, 3)));
        assertThat(game.getUnitAt(new Position(3, 2)).getOwner(), is(Player.BLUE));
    }

    @Test
    public void shouldSucceedAttack() {
        game.endOfTurn();
        game.moveUnit(new Position(3, 2), new Position(4, 3));
        assertNull(game.getUnitAt(new Position(3, 2)));
    }

    @Test
    public void shouldMakeRedWinner() {
        game.endOfTurn();
        game.changeProductionInCityAt(new Position(4, 1), GameConstants.ARCHER);
        for (int i = 0; i < 19; i++) {
            game.endOfTurn();
        }
        game.moveUnit(new Position(2, 0), new Position(3, 1));
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        game.moveUnit(new Position(3, 1), new Position(3, 2));
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        game.moveUnit(new Position(3, 2), new Position(4, 2));
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        assertThat(game.getWinner(), is(Player.RED));
    }

    @Test
    public void shouldWinAttackDueToCitySupport() {
        game.moveUnit(new Position(2, 0), new Position(1, 1));
        game.endOfTurn();
        game.moveUnit(new Position(3, 2), new Position(2, 1));
        game.endOfTurn();
        game.moveUnit(new Position(1, 1), new Position(2, 1));
        assertThat(game.getUnitAt(new Position(2, 1)).getOwner(), is(Player.RED));
    }
}
