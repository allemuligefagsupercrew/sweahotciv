package hotciv.standard;

import hotciv.framework.Player;
import hotciv.framework.Position;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class BetaCivSpecificTests extends BetaCivInit {
    //Wins

    @Test
    public void shouldMakeRedWinnerByTakingAllCities() {
        assertThat(game.moveUnit(new Position(4, 3), new Position(4, 2)), is(true));
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        assertThat(game.moveUnit(new Position(4, 2), new Position(4, 1)), is(true));
        assertThat(game.getCityAt(new Position(4, 1)).getOwner(), is(Player.RED));
        game.endOfTurn();
        assertThat(game.getWinner(), is(Player.RED));
    }

    @Test
    public void shouldMakeBlueWinnerByTakingAllCities() {
        game.endOfTurn();
        assertThat(game.moveUnit(new Position(3, 2), new Position(2, 1)), is(true));
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        assertThat(game.moveUnit(new Position(2, 1), new Position(1, 1)), is(true));
        assertThat(game.getCityAt(new Position(1, 1)).getOwner(), is(Player.BLUE));
        game.endOfTurn();
        assertThat(game.getWinner(), is(Player.BLUE));
    }

    // Aging

    @Test
    public void shouldBeYearNegative1() {
        for (int i = 0; i < 2 * 40; i++) {
            game.endOfTurn();
        }
        assertThat(game.getAge(), is(-1));
    }

    @Test
    public void shouldBeYear1() {
        for (int i = 0; i < 2 * 41; i++) {
            game.endOfTurn();
        }
        assertThat(game.getAge(), is(1));
    }

    @Test
    public void shouldBeYear50() {
        for (int i = 0; i < 2 * 42; i++) {
            game.endOfTurn();
        }
        assertThat(game.getAge(), is(50));
    }

    @Test
    public void shouldBeYear1750() {
        for (int i = 0; i < 2 * 76; i++) {
            game.endOfTurn();
        }
        assertThat(game.getAge(), is(1750));
    }

    @Test
    public void shouldBeYear1900() {
        for (int i = 0; i < 2 * 82; i++) {
            game.endOfTurn();
        }
        assertThat(game.getAge(), is(1900));
    }

    @Test
    public void shouldBeYear1970() {
        for (int i = 0; i < 2 * 96; i++) {
            game.endOfTurn();
        }
        assertThat(game.getAge(), is(1970));
    }

    @Test
    public void shouldBeYear1980() {
        for (int i = 0; i < 2 * 106; i++) {
            game.endOfTurn();
        }
        assertThat(game.getAge(), is(1980));
    }

}
