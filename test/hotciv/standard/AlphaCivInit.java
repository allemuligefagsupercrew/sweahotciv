package hotciv.standard;

import hotciv.standard.Factories.AlphaFactory;
import org.junit.Before;

public class AlphaCivInit {
    protected GameImpl game;

    @Before
    public void setUp() {
        game = new GameImpl(new AlphaFactory());
    }
}
