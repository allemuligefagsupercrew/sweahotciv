package hotciv.standard;

import hotciv.standard.Factories.SemiFactory;
import org.junit.Before;

public class SemiInit {
    protected GameImpl game;

    @Before
    public void setUp() {
        game = new GameImpl(new SemiFactory(
                new UnfairDieRollStrategy()));
    }
}
