package hotciv.standard;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;


/**
 * Skeleton class for AlphaCiv test cases
 * <p>
 * Updated Oct 2015 for using Hamcrest matchers
 * <p>
 * This source code is from the book
 * "Flexible, Reliable Software:
 * Using Patterns and Agile Development"
 * published 2010 by CRC Press.
 * Author:
 * Henrik B Christensen
 * Department of Computer Science
 * Aarhus University
 * <p>
 * Please visit http://www.baerbak.com/ for further information.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
@RunWith(value = Suite.class)
@Suite.SuiteClasses(value = {
        CityTestAlpha.class,
        TileTestAlpha.class,
        TurnsAndPlayersTestAlpha.class,
        UnitTestAlpha.class,
        BetaCivSpecificTests.class,
        GammaCivUnitActionTest.class,
        DeltaCivWorldLayoutTest.class,
        EpsilonCivTests.class,
        ZetaCivTestWinner.class,
        ThetaCivGalleyTests.class,
        EtaCivTests.class,
        SemiCivTests.class,
        GameObserverTest.class

})
public class TestSuiteHotCiv {


    /**
     * REMOVE ME. Not a test of HotCiv, just an example of what
     * matches the hamcrest library has...
     */

    /**
     @Test public void shouldDefinetelyBeRemoved() {
     // Matching null and not null values
     // 'is' require an exact match
     java.lang.String s = null;
     assertThat(s, is(nullValue()));
     s = "Ok";
     assertThat(s, is(notNullValue()));
     assertThat(s, is("Ok"));

     // If you only validate substrings, use containsString
     assertThat("This is a dummy test", containsString("dummy"));

     // Match contents of Lists
     List<java.lang.String> l = new ArrayList<java.lang.String>();
     l.add("Bimse");
     l.add("Bumse");
     // Note - ordering is ignored when matching using hasItems
     assertThat(l, hasItems(new java.lang.String[]{"Bumse", "Bimse"}));

     // Matchers may be combined, like is-not
     assertThat(l.get(0), is(not("Bumse")));
     } */
}
