package hotciv.standard;

import hotciv.framework.DieRollStrategy;

public class UnfairDieRollStrategy implements DieRollStrategy {
    private boolean high = true;

    @Override
    public int rollDie() {
        if (high) {
            high = !high;
            return 600;
        } else {
            high = !high;
            return 1;


        }
    }
}

