package hotciv.standard;

import hotciv.framework.GameConstants;
import hotciv.framework.Position;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class ThetaCivGalleyTests extends ThetaCivInit {

    @Test
    public void shouldProduceGalley() {
        game.changeProductionInCityAt(new Position(1, 1), GameConstants.GALLEY);
        for (int i = 0; i < 10; i++) {
            game.endOfTurn();
        }
        assertThat(game.getUnitAt(new Position(1, 0)).getTypeString(), is(GameConstants.GALLEY));
    }

    @Test
    public void shouldNotBeAbleToProduceGalley() {
        game.changeProductionInCityAt(new Position(4, 1), GameConstants.GALLEY);
        assertThat(GameConstants.GALLEY.equals(game.getCityAt(new Position(4, 1)).getProduction()), is(false));
    }

    @Test
    public void shouldCreateSettlerAndDestroyGalley() {
        game.changeProductionInCityAt(new Position(1, 1), GameConstants.GALLEY);
        for (int i = 0; i < 10; i++) {
            game.endOfTurn();
        }
        game.performUnitActionAt(new Position(1, 0));
        assertThat(game.getUnitAt(new Position(0, 0)).getTypeString(), is(GameConstants.SETTLER));
        assertNull(game.getUnitAt(new Position(1, 0)));
    }
}
