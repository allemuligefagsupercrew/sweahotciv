package hotciv.standard;

import hotciv.standard.Factories.EtaCivFactory;
import org.junit.Before;

public class EtaCivInit {
    protected GameImpl game;

    @Before
    public void setUp() {
        game = new GameImpl(new EtaCivFactory());
    }
}
