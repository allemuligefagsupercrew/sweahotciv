package hotciv.standard;

import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Position;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ZetaCivTestWinner extends ZetaCivInit {

    @Test
    public void shouldMakeRedWinnerAfterAtLeast21Turns() {
        game.endOfTurn();
        game.changeProductionInCityAt(new Position(4, 1), GameConstants.ARCHER);
        for (int i = 0; i < 39; i++) { //Skipping ahead 20 rounds.
            game.endOfTurn();
        }
        game.endOfTurn();
        for (int i = 0; i < 19; i++) {
            game.endOfTurn();
        }
        game.moveUnit(new Position(2, 0), new Position(3, 1));
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        game.moveUnit(new Position(3, 1), new Position(3, 2));
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        game.moveUnit(new Position(3, 2), new Position(4, 2));
        game.endOfTurn();
        assertThat(game.getWinner(), is(Player.RED));
    }

    @Test
    public void shouldMakeRedWinnerByTakingAllCitiesBefore20Rounds() {
        game.moveUnit(new Position(4, 3), new Position(4, 2));
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        assertThat(game.moveUnit(new Position(4, 2), new Position(4, 1)), is(true));
        assertThat(game.getCityAt(new Position(4, 1)).getOwner(), is(Player.RED));
        game.endOfTurn();
        assertThat(game.getWinner(), is(Player.RED));
    }
}
