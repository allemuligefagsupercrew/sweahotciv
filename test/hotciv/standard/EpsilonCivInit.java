package hotciv.standard;

import hotciv.framework.DieRollStrategy;
import hotciv.standard.Factories.EpsilonFactory;
import org.junit.Before;

/**
 * Created by Johannes on 01-10-2017.
 */
public class EpsilonCivInit {
    protected GameImpl game;

    @Before
    public void setUp() {


        game = new GameImpl(new EpsilonFactory(new UnfairDieRollStrategy()));
    }

}
