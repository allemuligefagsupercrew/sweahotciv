package hotciv.standard;

import hotciv.framework.GameConstants;
import hotciv.framework.Position;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class TileTestAlpha extends AlphaCivInit {
    @Test
    public void shouldBeOcean() {
        assertThat(game.getTileAt(new Position(1, 0)).getTypeString(), is(GameConstants.OCEANS));
    }

    @Test
    public void shouldBeMountain() {
        assertThat(game.getTileAt(new Position(2, 2)).getTypeString(), is(GameConstants.MOUNTAINS));
    }

    @Test
    public void shouldBePlains() {
        assertThat(game.getTileAt(new Position(2, 7)).getTypeString(), is(GameConstants.PLAINS));
    }

    @Test
    public void shouldBeHills() {
        assertThat(game.getTileAt(new Position(0, 1)).getTypeString(), is(GameConstants.HILLS));
    }

}
