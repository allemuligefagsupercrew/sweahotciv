package hotciv.standard;

import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.framework.Unit;
import org.hamcrest.core.Is;
import org.junit.Test;

import static junit.framework.TestCase.assertNull;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class GammaCivUnitActionTest extends GammaCivInit {

    @Test
    public void shouldChangeDefenseOfArcherFrom3to6andBack() {
        Position p = new Position(2, 0);
        Unit unit = game.getUnitAt(p);
        assertThat(unit.getTypeString(), is(GameConstants.ARCHER));
        game.performUnitActionAt(p);
        assertThat(unit.getDefensiveStrength(), is(6));
        game.performUnitActionAt(p);
        assertThat(unit.getDefensiveStrength(), is(3));
    }

    @Test
    public void shouldNotBeAbleToMoveFortifiedArcher() {
        game.performUnitActionAt(new Position(2, 0));
        game.moveUnit(new Position(2, 0), new Position(2, 1));
        assertNull(game.getUnitAt(new Position(2, 1)));
    }

    @Test
    public void shouldRemoveSettlerAndCreateNewCity() {
        Position p = new Position(4, 3);
        Unit unit = game.getUnitAt(p);
        assertThat(unit.getTypeString(), is(GameConstants.SETTLER));
        game.performUnitActionAt(p);

        assertThat(unit.getOwner(), is(Player.RED));

        unit = game.getUnitAt(p);
        assertNull(unit);
    }
}
