package hotciv.standard;

import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.framework.Unit;
import org.hamcrest.core.Is;
import org.junit.Test;

import static junit.framework.TestCase.assertNull;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SemiCivTests extends SemiInit {

    //Aging
    @Test
    public void shouldBeYearNegative1() {
        for (int i = 0; i < 2 * 40; i++) {
            game.endOfTurn();
        }
        assertThat(game.getAge(), is(-1));
    }

    @Test
    public void shouldBeYear1() {
        for (int i = 0; i < 2 * 41; i++) {
            game.endOfTurn();
        }
        assertThat(game.getAge(), is(1));
    }

    @Test
    public void shouldBeYear50() {
        for (int i = 0; i < 2 * 42; i++) {
            game.endOfTurn();
        }
        assertThat(game.getAge(), is(50));
    }

    @Test
    public void shouldBeYear1750() {
        for (int i = 0; i < 2 * 76; i++) {
            game.endOfTurn();
        }
        assertThat(game.getAge(), is(1750));
    }

    @Test
    public void shouldBeYear1900() {
        for (int i = 0; i < 2 * 82; i++) {
            game.endOfTurn();
        }
        assertThat(game.getAge(), is(1900));
    }

    @Test
    public void shouldBeYear1970() {
        for (int i = 0; i < 2 * 96; i++) {
            game.endOfTurn();
        }
        assertThat(game.getAge(), is(1970));
    }

    @Test
    public void shouldBeYear1980() {
        for (int i = 0; i < 2 * 106; i++) {
            game.endOfTurn();
        }
        assertThat(game.getAge(), is(1980));
    }

    //Settler Action
    @Test
    public void shouldRemoveSettlerAndCreateNewCity() {
        Position p = new Position(5, 5);
        Unit unit = game.getUnitAt(p);
        assertThat(unit.getTypeString(), is(GameConstants.SETTLER));
        game.performUnitActionAt(p);
        assertThat(unit.getOwner(), is(Player.RED));
        unit = game.getUnitAt(p);
        assertNull(unit);
    }


    //World
    @Test
    public void shouldBeRedPlayersCityAtPosition8dot12() {
        assertThat(game.getCityAt(new Position(8, 12)).getOwner(), is(Player.RED));
    }

    @Test
    public void shouldBeBluePlayersCityAtPosition4dot5() {
        assertThat(game.getCityAt(new Position(4, 5)).getOwner(), is(Player.BLUE));
    }

    @Test
    public void shouldBeOceanTileAtPosition1dot1() {
        assertThat(game.getTileAt(new Position(0, 1)).getTypeString(), is(GameConstants.OCEANS));
    }

    @Test
    public void shouldBeMountainTileAtPosition0dot6() {
        assertThat(game.getTileAt(new Position(0, 5)).getTypeString(), is(GameConstants.MOUNTAINS));
    }

    //Winner
    @Test
    public void shouldMakeBlueWinner() {//Cities at 4,5(BLUE) & 8,12(RED)
        game.changeProductionInCityAt(new Position(8, 12), GameConstants.ARCHER);
        game.endOfTurn();
        game.moveUnit(new Position(4, 4), new Position(5, 4));
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        game.moveUnit(new Position(5, 4), new Position(6, 4));
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        game.moveUnit(new Position(6, 4), new Position(7, 4));
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        game.moveUnit(new Position(7, 4), new Position(8, 4));
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        game.moveUnit(new Position(8, 4), new Position(9, 4));
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        game.moveUnit(new Position(9, 4), new Position(9, 5));
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        game.moveUnit(new Position(9, 5), new Position(9, 6));
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        game.moveUnit(new Position(9, 6), new Position(8, 7));
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        game.moveUnit(new Position(8, 7), new Position(8, 8));
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        game.moveUnit(new Position(8, 8), new Position(8, 9));
        for (int i = 0; i < 40; i++) {
            game.endOfTurn();
        }
        game.moveUnit(new Position(8, 9), new Position(8, 10));
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        game.moveUnit(new Position(8, 10), new Position(8, 11));
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        game.moveUnit(new Position(8, 11), new Position(9, 11));
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        game.moveUnit(new Position(9, 11), new Position(9, 12));
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        game.endOfTurn();
        assertThat(game.getWinner(), is(Player.BLUE));
    }

    //Attacking

    @Test
    public void shouldFailAttackAndDestroySettler() {
        game.moveUnit(new Position(5, 5), new Position(4, 4));
        assertNull(game.getUnitAt(new Position(5, 5)));
        assertThat(game.getUnitAt(new Position(4, 4)).getOwner(), Is.is(Player.BLUE));
    }


    @Test
    public void cityShouldBeSize2() {
        for (int i = 0; i < 16; i++) {
            game.endOfTurn();
        }
        assertThat(game.getCityAt(new Position(8, 12)).getSize(), Is.is(2));

    }

    @Test
    public void cityShouldBeSize3() {
        game.changeWorkForceFocusInCityAt(new Position(4, 5), GameConstants.foodFocus);
        for (int i = 0; i < 16 + 6; i++) {
            game.endOfTurn();
        }
        assertThat(game.getCityAt(new Position(4, 5)).getSize(), is(3));

    }

    @Test
    public void cityShouldBeSize2DueToProductionFocus() {
        for (int i = 0; i < 16 + 6; i++) {
            game.endOfTurn();
        }
        assertThat(game.getCityAt(new Position(4, 5)).getSize(), is(2));

    }
    //TODO: TEST PRODUCTION
}
