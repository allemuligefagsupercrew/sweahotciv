package hotciv.standard;

import hotciv.framework.City;
import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Position;
import org.junit.Test;

import static junit.framework.TestCase.assertNull;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class CityTestAlpha extends AlphaCivInit {

    @Test
    public void shouldHaveRedCityAtCoordinates() {
        City city = game.getCityAt(new Position(1, 1));
        assertNotNull(city);
        assertThat(city.getOwner(), is(Player.RED));
    }

    @Test
    public void shouldHaveBlueCityAtCoordinates() {
        City city = game.getCityAt(new Position(4, 1));
        assertNotNull(city);
        assertThat(city.getOwner(), is(Player.BLUE));
    }

    @Test
    public void shouldHaveCityWithSizeOne() {
        City city = game.getCityAt(new Position(1, 1));
        assertNotNull(city);
        assertThat(city.getSize(), is(1));
    }


    @Test
    public void shouldNotHaveCityAtCoordinates() {
        City city = game.getCityAt(new Position(3, 5));
        assertNull(city);
    }

    @Test
    public void shouldHaveBlueCityProductionBeZero() {
        assertThat(game.getCityAt(new Position(1, 1)).getAvailableProduction(), is(0));
    }

    @Test
    public void shouldHaveRedCityProductionBeZero() {
        assertThat(game.getCityAt(new Position(1, 1)).getAvailableProduction(), is(0));
    }

    @Test
    public void shouldIncrementProductionBySixEachRoundForRedCity() {
        game.endOfTurn();
        game.endOfTurn();
        assertThat(game.getCityAt(new Position(1, 1)).getAvailableProduction(), is(6));
    }

    @Test
    public void shouldProduceRedArcherAfterTwoRounds() {
        game.changeProductionInCityAt(new Position(1, 1), GameConstants.ARCHER);
        for (int i = 0; i < 4; i++) {
            game.endOfTurn();
        }
        assertThat(game.getUnitAt(new Position(1, 1)).getTypeString(), is(GameConstants.ARCHER));
    }

    @Test
    public void shouldProduceBlueSettlerAfterFiveRounds() {
        game.endOfTurn();
        game.changeProductionInCityAt(new Position(4, 1), GameConstants.SETTLER);
        for (int i = 0; i < 10; i++) {
            game.endOfTurn();
        }
        assertThat(game.getUnitAt(new Position(4, 1)).getTypeString(), is(GameConstants.SETTLER));
    }

    @Test
    public void shouldHaveZeroProductionAfterSettlerIsProduced() {
        game.endOfTurn();
        game.changeProductionInCityAt(new Position(4, 1), GameConstants.SETTLER);
        for (int i = 0; i < 10; i++) {
            game.endOfTurn();
        }
        assertThat(game.getCityAt(new Position(4, 1)).getAvailableProduction(), is(0));
    }

    @Test
    public void shouldOnlyBeRedWhoCanProduceUnitsInRedsTurn() {
        game.changeProductionInCityAt(new Position(4, 1), GameConstants.SETTLER);
        assertNull(game.getCityAt(new Position(4, 1)).getProduction());
    }

    @Test
    public void shouldProduceUnitsInAndNorthOfCity() {
        game.changeProductionInCityAt(new Position(1, 1), GameConstants.LEGION);
        for (int i = 0; i < 6; i++) {
            game.endOfTurn();
        }
        assertThat(game.getUnitAt(new Position(1, 1)).getTypeString(), is(GameConstants.LEGION));
        game.changeProductionInCityAt(new Position(1, 1), GameConstants.ARCHER);
        for (int i = 0; i < 4; i++) {
            game.endOfTurn();
        }
        assertThat(game.getUnitAt(new Position(0, 1)).getTypeString(), is(GameConstants.ARCHER));
    }


    @Test
    public void shouldMakeBlueCityRed() {
        game.moveUnit(new Position(2, 0), new Position(3, 1));
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        assertThat(game.moveUnit(new Position(3, 1), new Position(4, 1)), is(true));
        assertThat(game.getCityAt(new Position(4, 1)).getOwner(), is(Player.RED));
    }


}
