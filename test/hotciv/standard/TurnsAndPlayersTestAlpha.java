package hotciv.standard;

import hotciv.framework.Player;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class TurnsAndPlayersTestAlpha extends AlphaCivInit {

    @Test
    public void shouldNotHaveGameBeNull() {
        assertThat(game, is(notNullValue()));
    }

    @Test
    public void shouldBeRedAsStartingPlayer() {
        assertThat(game.getPlayerInTurn(), is(Player.RED));
    }

    @Test
    public void shouldBeBlueAfterRedEndTurn() {
        game.endOfTurn();
        assertThat(game.getPlayerInTurn(), is(Player.BLUE));
    }

    @Test
    public void shouldBeRedAfterBlueEndTurn() {
        game.endOfTurn();
        game.endOfTurn();
        assertThat(game.getPlayerInTurn(), is(Player.RED));
    }

    @Test
    public void shouldHaveWinnerBeNull() {
        assertNull(game.getWinner());
    }

    @Test
    public void shouldBeYearNegative4000() {
        assertThat(game.getAge(), is(-4000));
    }

    @Test
    public void shouldBeYearNegative3900() {
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        assertThat(game.getAge(), is(-3900));
    }

    @Test
    public void shouldBeYearNegative100() {
        for (int i = 0; i < 2 * 39; i++) {
            game.endOfTurn();
        }
        assertThat(game.getAge(), is(-100));
    }

    @Test
    public void shouldBeRedWinner() {
        for (int i = 0; i < 10; i++) {
            game.endOfTurn();
            game.endOfTurn();
        }
        assertThat(game.getWinner(), is(Player.RED));
    }

}
