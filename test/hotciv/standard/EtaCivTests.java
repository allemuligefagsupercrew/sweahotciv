package hotciv.standard;

import hotciv.framework.GameConstants;
import hotciv.framework.Position;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class EtaCivTests extends EtaCivInit {

    @Test
    public void cityShouldBeSize2() {
        for (int i = 0; i < 16; i++) {
            game.endOfTurn();
        }
        assertThat(game.getCityAt(new Position(1, 1)).getSize(), is(2));

    }

    @Test
    public void cityShouldBeSize3() {
        game.changeWorkForceFocusInCityAt(new Position(1, 1), GameConstants.foodFocus);
        for (int i = 0; i < 16 + 6; i++) {
            game.endOfTurn();
        }
        assertThat(game.getCityAt(new Position(1, 1)).getSize(), is(3));

    }

    @Test
    public void cityShouldBeSize2DueToProductionFocus() {
        for (int i = 0; i < 16 + 6; i++) {
            game.endOfTurn();
        }
        assertThat(game.getCityAt(new Position(1, 1)).getSize(), is(2));

    }

    //TODO: TEST PRODUCTION

}
