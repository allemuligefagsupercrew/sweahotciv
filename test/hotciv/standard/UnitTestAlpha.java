package hotciv.standard;

import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.framework.Unit;
import org.junit.Test;

import static junit.framework.TestCase.assertNull;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class UnitTestAlpha extends AlphaCivInit {
    @Test
    public void shouldBeRedArcher() {
        Unit unit = game.getUnitAt(new Position(2, 0));
        assertThat(unit.getOwner(), is(Player.RED));
        assertThat(unit.getTypeString(), is(GameConstants.ARCHER));
    }

    @Test
    public void shouldBeBlueLegion() {
        Unit unit = game.getUnitAt(new Position(3, 2));
        assertThat(unit.getOwner(), is(Player.BLUE));
        assertThat(unit.getTypeString(), is(GameConstants.LEGION));
    }

    @Test
    public void shouldBeRedSettler() {
        Unit unit = game.getUnitAt(new Position(4, 3));
        assertThat(unit.getOwner(), is(Player.RED));
        assertThat(unit.getTypeString(), is(GameConstants.SETTLER));
    }

    @Test
    public void shouldAllHaveMoveCountOne() {
        //Red archer
        assertThat(game.getUnitAt(new Position(2, 0)).getMoveCount(), is(1));
        //Red Settler
        assertThat(game.getUnitAt(new Position(4, 3)).getMoveCount(), is(1));
        //Blue Legion
        assertThat(game.getUnitAt(new Position(3, 2)).getMoveCount(), is(1));
    }

    @Test
    public void shouldMoveRedArcher() {
        assertThat(game.moveUnit(new Position(2, 0), new Position(2, 1)), is(true));
    }

    @Test
    public void shouldBeBlueCantMoveRedArcher() {
        game.endOfTurn();
        Unit unit = game.getUnitAt(new Position(2, 0));
        assertThat(game.moveUnit(new Position(2, 0), new Position(2, 1)), is(false));
        assertThat(unit.getMoveCount(), is(1));
    }

    @Test
    public void shouldBeAbleToMoveUnitAgainNextTurn() {
        Unit unit = game.getUnitAt(new Position(2, 0));
        assertThat(game.moveUnit(new Position(2, 0), new Position(2, 1)), is(true));
        assertThat(unit.getMoveCount(), is(0));
        game.endOfTurn();
        game.endOfTurn();
        assertThat(unit.getMoveCount(), is(1));
        assertThat(game.moveUnit(new Position(2, 1), new Position(2, 0)), is(true));
        assertThat(unit.getMoveCount(), is(0));
    }

    @Test
    public void shouldNotBeAbleToMoveTwoTiles() {
        Unit unit = game.getUnitAt(new Position(2, 0));
        assertThat(game.moveUnit(new Position(2, 0), new Position(4, 4)), is(false));
    }

    @Test
    public void shouldNotMoveTileOccupiedByFriendlyUnit() {
        assertThat(game.moveUnit(new Position(2, 0), new Position(3, 1)), is(true));
        assertThat(game.moveUnit(new Position(4, 3), new Position(3, 3)), is(true));

        game.endOfTurn();

        //Has to move blue unit, as it is in the way
        assertThat(game.moveUnit(new Position(3, 2), new Position(4, 2)), is(true));

        game.endOfTurn();

        //Attemps to move the two red units to position 3,2
        assertThat(game.moveUnit(new Position(3, 1), new Position(3, 2)), is(true));
        assertThat(game.moveUnit(new Position(3, 3), new Position(3, 2)), is(false));

    }

    @Test
    public void shouldNotBeAbleToMoveUntoOceanTile() {
        assertThat(game.moveUnit(new Position(2, 0), new Position(1, 0)), is(false));

    }


    @Test
    public void shouldNotBeAbleToMoveUntoMountainTile() {
        game.endOfTurn();
        assertThat(game.moveUnit(new Position(3, 2), new Position(2, 2)), is(false));
    }

    @Test
    public void shouldMakeRedSettlerDestroyBlueLegion() {
        assertThat(game.moveUnit(new Position(4, 3), new Position(3, 2)), is(true));
    }

    @Test
    public void shouldMakeBlueLegionDestroyRedSettler() {
        game.endOfTurn();
        assertThat(game.moveUnit(new Position(3, 2), new Position(4, 3)), is(true));
    }

    @Test
    public void shouldBeNullUnit() {
        assertNull(game.getUnitAt(new Position(0, 0)));
    }

}
