package hotciv.standard;

import hotciv.framework.Factory;
import hotciv.framework.Position;
import hotciv.standard.Factories.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class GameObserverTest {

    @Parameterized.Parameter
    public Factory factory;

    @Parameterized.Parameters(name = "{0}")
    public static List<Factory> getFactories() {
        return Arrays.asList(
                new AlphaFactory(),
                new BetaFactory(),
                new EpsilonFactory(new UnfairDieRollStrategy()),
                new EtaCivFactory(),
                new GammaFactory(),
                new ZetaFactory(),
                new ThetaFactory());
    }

    @Test
    public void shouldTestGameObserver() {
        GameImpl game = new GameImpl(factory);
        GameObserverTestSpy gameObserverTestSpy = new GameObserverTestSpy();
        game.addObserver(gameObserverTestSpy);

        game.moveUnit(new Position(4, 3), new Position(4, 2));
        for (int i = 0; i < 2; i++) {
            game.endOfTurn();
        }
        assertThat(gameObserverTestSpy.getChangeSet().size(), is(2));
        assertThat(gameObserverTestSpy.getEndOfTurnObjects().size(), is(2));

        game.moveUnit(new Position(4, 2), new Position(4, 1));
        Set<Position> changeSet = gameObserverTestSpy.getChangeSet();
        assertTrue(changeSet.contains(new Position(4, 2)));
        assertThat(changeSet.size(), is(3));
    }
}
