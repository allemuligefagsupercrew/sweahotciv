package hotciv.standard;

import hotciv.framework.*;
import org.junit.Test;

import static junit.framework.TestCase.assertNull;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class DeltaCivWorldLayoutTest extends DeltaCivInit{

    @Test
    public void shouldBeRedPlayersCityAtPosition8dot12(){
        assertThat(game.getCityAt(new Position(8,12)).getOwner(), is(Player.RED));
    }

    @Test
    public void shouldBeBluePlayersCityAtPosition4dot5(){
        assertThat(game.getCityAt(new Position(4,5)).getOwner(), is(Player.BLUE));
    }

    @Test
    public void shouldBeOceanTileAtPosition1dot1(){
        assertThat(game.getTileAt(new Position(0,1)).getTypeString(), is(GameConstants.OCEANS));
    }

    @Test
    public void shouldBeMountainTileAtPosition0dot6(){
        assertThat(game.getTileAt(new Position(0,5)).getTypeString(), is(GameConstants.MOUNTAINS));
    }

}
