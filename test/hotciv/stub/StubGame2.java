package hotciv.stub;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import hotciv.framework.*;
import hotciv.standard.UnitImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Test stub for game for visual testing of
 * minidraw based graphics.
 * <p>
 * SWEA support code.
 * <p>
 * This source code is from the book
 * "Flexible, Reliable Software:
 * Using Patterns and Agile Development"
 * published 2010 by CRC Press.
 * Author:
 * Henrik B Christensen
 * Department of Computer Science
 * Aarhus University
 * <p>
 * Please visit http://www.baerbak.com/ for further information.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class StubGame2 implements Game {

    // === Observer handling ===
    protected GameObserver gameObserver;
    // A simple implementation to draw the map of DeltaCiv
    protected Map<Position, Tile> world;
    // === Unit handling ===
    private Position pos_archer_red;
    private Position pos_legion_blue;
    private Position pos_settler_red;
    private Position pos_galley_red;
    private Unit red_archer;
    // === Turn handling ===
    private Player inTurn;
    // === City handling ===
    private City city;
    private Position city_pos;

    public StubGame2() {
        defineWorld(1);
        // AlphaCiv configuration
        pos_archer_red = new Position(2, 0);
        pos_legion_blue = new Position(3, 2);
        pos_settler_red = new Position(4, 3);
        pos_galley_red = new Position(6, 4);

        // the only one I need to store for this stub
        red_archer = new StubUnit(GameConstants.ARCHER, Player.RED);

        city = new StubCity(Player.BLUE);
        city_pos = new Position(2, 2);

        inTurn = Player.RED;
    }

    public Unit getUnitAt(Position p) {
        if (p.equals(pos_archer_red)) {
            return red_archer;
        }
        if (p.equals(pos_settler_red)) {
            return new StubUnit(GameConstants.SETTLER, Player.RED);
        }
        if (p.equals(pos_legion_blue)) {
            return new StubUnit(GameConstants.LEGION, Player.BLUE);
        }
        if (p.equals(pos_galley_red)) {
            return new StubUnit(ThetaConstants.GALLEY, Player.RED);
        }
        return null;
    }

    @Override
    public void addAttackObserver(AttackObserver attackObserver) {

    }

    @Override
    public void addTranscribingObserver(TranscribingObserver transcribingObserver) {

    }

    @Override
    public void activateTranscribingListeners() {

    }

    @Override
    public void deactivateTranscribingListeners() {

    }

    // Stub only allows moving red archer
    public boolean moveUnit(Position from, Position to) {
        System.out.println("-- StubGame2 / moveUnit called: " + from + "->" + to);
        if (from.equals(pos_archer_red)) {
            pos_archer_red = to;
        }
        if (to.equals(city_pos)) {
            city.setOwner(Player.RED);
        }
        // notify our observer(s) about the changes on the tiles
        gameObserver.worldChangedAt(from);
        gameObserver.worldChangedAt(to);
        return true;
    }

    public void endOfTurn() {
        System.out.println("-- StubGame2 / endOfTurn called.");
        inTurn = (getPlayerInTurn() == Player.RED ?
                Player.BLUE :
                Player.RED);
        // no age increments
        gameObserver.turnEnds(inTurn, -4000);
    }

    public Player getPlayerInTurn() {
        return inTurn;
    }

    // observer list is only a single one...
    public void addObserver(GameObserver observer) {
        gameObserver = observer;
    }

    public Tile getTileAt(Position p) {
        return world.get(p);
    }


    /**
     * define the world.
     *
     * @param worldType 1 gives one layout while all other
     *                  values provide a second world layout.
     */
    protected void defineWorld(int worldType) {
        world = new HashMap<Position, Tile>();
        for (int r = 0; r < GameConstants.WORLDSIZE; r++) {
            for (int c = 0; c < GameConstants.WORLDSIZE; c++) {
                Position p = new Position(r, c);
                world.put(p, new StubTile(GameConstants.PLAINS));
            }
        }
    }

    public City getCityAt(Position p) {
        if (city_pos.equals(p)) {
            return city;
        }
        return null;
    }

    public Player getWinner() {
        return null;
    }

    public int getAge() {
        return 0;
    }

    public void changeWorkForceFocusInCityAt(Position p, String balance) {
    }

    public void changeProductionInCityAt(Position p, String unitType) {
    }

    public void performUnitActionAt(Position p) {
        System.out.println("The unit at position " + p + " is now performing an action!");
    }

    public void setTileFocus(Position position) {
        gameObserver.tileFocusChangedAt(position);
    }

}

class StubUnit implements Unit {
    private String type;
    private Player owner;

    public StubUnit(String type, Player owner) {
        this.type = type;
        this.owner = owner;
    }

    public String getTypeString() {
        return type;
    }

    public Player getOwner() {
        return owner;
    }

    public int getMoveCount() {
        return 1;
    }

    public int getDefensiveStrength() {
        return 0;
    }

    @Override
    public void setDefensiveStrength(int def) {

    }

    public int getAttackingStrength() {
        return 0;
    }

    @Override
    public List<String> getCompatibleTileTypes() {
        return null;
    }

    @Override
    public boolean isFortified() {
        return false;
    }

}

class StubCity implements City {

    private Player owner;

    public StubCity(Player owner) {
        this.owner = owner;
    }

    /**
     * return the owner of this city.
     *
     * @return the player that controls this city.
     */
    @Override
    public Player getOwner() {
        return owner;
    }

    /**
     * Sets new owner for the city
     *
     * @param player is the new owner of this city
     */
    @Override
    public void setOwner(Player player) {
        owner = player;
    }

    /**
     * return the size of the population.
     *
     * @return population size.
     */
    @Override
    public int getSize() {
        return 0;
    }

    /**
     * return the type of unit this city is currently producing.
     *
     * @return a string type defining the unit under production,
     * see GameConstants for valid values.
     */
    @Override
    public String getProduction() {
        return GameConstants.ARCHER;
    }

    /**
     * Changes production focus for city
     *
     * @param unitType the type of unit to be produced
     */
    @Override
    public void setProduction(String unitType) {

    }

    /**
     * return the work force's focus in this city.
     *
     * @return a string type defining the focus, see GameConstants
     * for valid return values.
     */
    @Override
    public String getWorkforceFocus() {
        return GameConstants.foodFocus;
    }

    /**
     * @param balance the new workforce focus
     */
    @Override
    public void setWorkforceFocus(String balance) {

    }

    /**
     * @return whether the city has enough production to produce the unit it's working on or not.
     */
    @Override
    public boolean hasEnoughProduction() {
        return false;
    }

    /**
     * Retrieves the unit a city is producing.
     * Removes production.
     *
     * @return the unit produced
     */
    @Override
    public UnitImpl retrieveProducedUnit() {
        return null;
    }

    /**
     * @return the amount of production the city currently has
     */
    @Override
    public int getAvailableProduction() {
        return 0;
    }

    /**
     * Makes the City Grow
     * <p>
     * Should ONLY be used by {@link CityGrowthStrategy}
     */
    @Override
    public void grow() {

    }
}