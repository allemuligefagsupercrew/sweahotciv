package hotciv.visual;

import hotciv.framework.Game;
import hotciv.standard.Factories.SemiFactory;
import hotciv.standard.GameImpl;
import hotciv.standard.strategy.RandomDieRoll;
import hotciv.view.tools.*;
import minidraw.framework.DrawingEditor;
import minidraw.standard.MiniDrawApplication;

import java.util.Arrays;

public class ShowSemi {
    public static void main(String[] args) {
        Game game = new GameImpl(new SemiFactory(new RandomDieRoll()));

        DrawingEditor editor =
                new MiniDrawApplication("SemiCiv",
                        new HotCivFactory4(game));
        editor.open();

        editor.setTool(new CompositionTool(editor, Arrays.asList(new ActionTool(editor, game), new EndOfTurnTool(editor, game), new SetFocusTool(editor, game), new UnitMoveTool(editor, game))));
    }
}
