# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Repository is for the HotCiv project in the SWEA course on the AU Computer Science Bachelors Programme

### How do I get set up? ###

Use ant to test or build. See build.xml for available targets

### Contribution guidelines ###

Write tests for all methods.
Check code coverage often
Always describe changes in commit message

### Who do I talk to? ###

Johannes Ernstsen or Mathias Søby Jensen